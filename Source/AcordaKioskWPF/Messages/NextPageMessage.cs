﻿using System;

namespace AcordaKioskWPF.Messages
{
    public class NextPageMessage
    {
        public NextPageMessage(Type source) { SourcePage = source.ToString(); }
        public NextPageMessage(string source) { SourcePage = source; }

        public string SourcePage { get; private set; }
    }
}
