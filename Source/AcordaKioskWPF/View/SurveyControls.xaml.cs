﻿using System;
using System.Windows;
using System.Windows.Controls;
using AcordaKioskWPF.Messages;
using GalaSoft.MvvmLight.Messaging;
using AcordaKioskWPF.ViewModel;
using AcordaKioskWPF.Model;
using System.Windows.Media.Imaging;

namespace AcordaKioskWPF.View
{
    /// <summary>
    /// Interaction logic for SurveyControls.xaml
    /// </summary>
    public partial class SurveyControls : UserControl
    {
        public const int NUM_SURVEY_PAGES = 6;

        public int PageNumber
        {
            get { return (int)GetValue(PageNumberProperty); }
            set { SetValue(PageNumberProperty, value); }
        }
        public static readonly DependencyProperty PageNumberProperty =
            DependencyProperty.Register(
                "PageNumber", 
                typeof(int), 
                typeof(SurveyControls), 
                new PropertyMetadata(1, PageNumberChanged));

        private static void PageNumberChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            if (args.NewValue == null) return;

            var ctrl = (SurveyControls) obj;
            var pgnum = (int) args.NewValue;

            if (pgnum == 1)
            {
                HideAllAnsweredQuestionBoxes(ctrl);
            }

            if (pgnum <= 2)
                ctrl.PreviousVisible = false;
            if (pgnum > 2)
                ctrl.PreviousVisible = true;

            switch (pgnum)
            {
                case 0:
                case 1:
                case 2:
                    SetAllPageButtonsToInactive(ctrl);
                    ctrl.btnPage1Image.Source = new BitmapImage(new Uri("pack://siteoforigin:,,,/Resources/Images/Survey/btnSurveyQuestionBoxCurrent.png"));
                    break;
                case 3:
                    SetAllPageButtonsToInactive(ctrl);
                    ctrl.btnPage2Image.Source = new BitmapImage(new Uri("pack://siteoforigin:,,,/Resources/Images/Survey/btnSurveyQuestionBoxCurrent.png"));
                    break;
                case 4:
                    SetAllPageButtonsToInactive(ctrl);
                    ctrl.btnPage3Image.Source = new BitmapImage(new Uri("pack://siteoforigin:,,,/Resources/Images/Survey/btnSurveyQuestionBoxCurrent.png"));
                    break;
                case 5:
                    SetAllPageButtonsToInactive(ctrl);
                    ctrl.btnPage4Image.Source = new BitmapImage(new Uri("pack://siteoforigin:,,,/Resources/Images/Survey/btnSurveyQuestionBoxCurrent.png"));
                    break;
                case 6:
                    SetAllPageButtonsToInactive(ctrl);
                    ctrl.btnPage5Image.Source = new BitmapImage(new Uri("pack://siteoforigin:,,,/Resources/Images/Survey/btnSurveyQuestionBoxCurrent.png"));
                    break;
            }

            

            //width = (pgnum - 3) * 49 + 22;
            //ctrl.CheckMaskWidth = width;

            //ctrl.btnPage1.Visibility = Visibility.Hidden;

            //ctrl.imgSurveySquares.Source = new BitmapImage(new Uri("pack://siteoforigin:,,,/Resources/bgSurveySquares.png"));
            //ctrl.imgSurveySquaresFilled.Source = new BitmapImage(new Uri("pack://siteoforigin:,,,/Resources/bgSurveySquaresFilled.png"));

        }

        private static void SetAllPageButtonsToInactive(SurveyControls ctrl)
        {
            var inactiveQuestionBoxPath = "pack://siteoforigin:,,,/Resources/Images/Survey/btnSurveyQuestionBox.png";
            ctrl.btnPage1Image.Source = new BitmapImage(new Uri(inactiveQuestionBoxPath));
            ctrl.btnPage2Image.Source = new BitmapImage(new Uri(inactiveQuestionBoxPath));
            ctrl.btnPage3Image.Source = new BitmapImage(new Uri(inactiveQuestionBoxPath));
            ctrl.btnPage4Image.Source = new BitmapImage(new Uri(inactiveQuestionBoxPath));
            ctrl.btnPage5Image.Source = new BitmapImage(new Uri(inactiveQuestionBoxPath));
        }

        public string SurveyControlUserType;
        public string ControlUserType
        {
            get { return (string)GetValue(CurrentUserTypeProperty); }
            set { SetValue(CurrentUserTypeProperty, value); }
        }
        public static readonly DependencyProperty CurrentUserTypeProperty = DependencyProperty.Register(
            "ControlUserType", 
            typeof(string), 
            typeof(SurveyControls), 
            new PropertyMetadata(CurrentUserTypeChanged));

        private static void CurrentUserTypeChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            if (args.NewValue == null) return;
            var ctrl = (SurveyControls)obj;
            ctrl.SurveyControlUserType = (string)args.NewValue;
        }

        public bool NextVisible
        {
            get { return (bool)GetValue(NextVisibleProperty); }
            set { SetValue(NextVisibleProperty, value); }
        }
        public static readonly DependencyProperty NextVisibleProperty = DependencyProperty.Register("NextVisible", typeof(bool), typeof(SurveyControls), new PropertyMetadata(true));

        public bool PreviousVisible
        {
            get { return (bool)GetValue(PreviousVisibleProperty); }
            set { SetValue(PreviousVisibleProperty, value); }
        }
        public static readonly DependencyProperty PreviousVisibleProperty =
            DependencyProperty.Register("PreviousVisible", typeof(bool), typeof(SurveyControls), new PropertyMetadata(false));

        public double CheckMaskHeight
        {
            get { return (double)GetValue(CheckMaskHeightProperty); }
            set { SetValue(CheckMaskHeightProperty, value); }
        }
        public static readonly DependencyProperty CheckMaskHeightProperty =
            DependencyProperty.Register("CheckMaskHeight", typeof(double), typeof(SurveyControls), new PropertyMetadata(100.0));

        public double CheckMaskWidth
        {
            get { return (double)GetValue(CheckMaskWidthProperty); }
            set { SetValue(CheckMaskWidthProperty, value); }
        }
        public static readonly DependencyProperty CheckMaskWidthProperty =
            DependencyProperty.Register("CheckMaskWidth", typeof(double), typeof(SurveyControls), new PropertyMetadata(19.0));

        public bool OkVisible
        {
            get { return (bool)GetValue(OkVisibleProperty); }
            set { SetValue(OkVisibleProperty, value); }
        }
        public static readonly DependencyProperty OkVisibleProperty =
            DependencyProperty.Register("OkVisible", typeof(bool), typeof(SurveyControls), new PropertyMetadata(false));

        public string QuestionIdWithAnswer
        {
            get { return (string)GetValue(QuestionIdWithAnswerProperty); }
            set { SetValue(QuestionIdWithAnswerProperty, value); }
        }

        public static readonly DependencyProperty QuestionIdWithAnswerProperty =
            DependencyProperty.Register("QuestionIdWithAnswer", typeof(string), typeof(SurveyControls), new PropertyMetadata(QuestionIdWithAnswerChanged));

        private static void QuestionIdWithAnswerChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            if (args.NewValue == null) return;
            var ctrl = (SurveyControls)obj;
            var questionIdsString = (string)args.NewValue;
            HideAllAnsweredQuestionBoxes(ctrl);

            var questionIds = questionIdsString.ToCharArray();
            foreach (var questionId in questionIds)
            {
                switch (questionId)
                {
                    case '3':
                        ctrl.btnPage1AnsweredImage.Visibility = Visibility.Visible;
                        break;
                    case '4':
                        ctrl.btnPage2AnsweredImage.Visibility = Visibility.Visible;
                        break;
                    case '5':
                        ctrl.btnPage3AnsweredImage.Visibility = Visibility.Visible;
                        break;
                    case '6':
                        ctrl.btnPage4AnsweredImage.Visibility = Visibility.Visible;
                        break;
                    case '7':
                        ctrl.btnPage5AnsweredImage.Visibility = Visibility.Visible;
                        break;
                }
            }
        }

        private static void HideAllAnsweredQuestionBoxes(SurveyControls ctrl)
        {
            ctrl.btnPage1AnsweredImage.Visibility = Visibility.Hidden;
            ctrl.btnPage2AnsweredImage.Visibility = Visibility.Hidden;
            ctrl.btnPage3AnsweredImage.Visibility = Visibility.Hidden;
            ctrl.btnPage4AnsweredImage.Visibility = Visibility.Hidden;
            ctrl.btnPage5AnsweredImage.Visibility = Visibility.Hidden;
        }

        public SurveyControls()
        {
            InitializeComponent();
        }


        private void OkButton_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Messenger.Default.Send(new AnswerAcceptedMessage());
            if(PageNumber >= NUM_SURVEY_PAGES)
                Messenger.Default.Send(new SurveyFinishedMessage());
        }

        private void NextButton_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            //Messenger.Default.Send(new SurveyQuestionSelectedMessage());    // send a blank question response to clear out before moving forward
            Messenger.Default.Send(new AnswerAcceptedMessage());

            if (PageNumber >= NUM_SURVEY_PAGES)
                Messenger.Default.Send(new SurveyFinishedMessage());
        }

        private void PrevButton_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Messenger.Default.Send(new PreviousQuestionMessage());
        }

        private void btnPage5_Click(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send(new NextPageMessage(Pages.Question6));
        }

        private void btnPage4_Click(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send(new NextPageMessage(Pages.Question5));
        }

        private void btnPage3_Click(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send(new NextPageMessage(Pages.Question4));
        }

        private void btnPage2_Click(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send(new NextPageMessage(Pages.Question3));
        }

        private void btnPage1_Click(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send(new NextPageMessage(Pages.Question2));
        }

    }
}
