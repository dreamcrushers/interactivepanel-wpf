﻿using System.Windows;
using System.Windows.Controls;

namespace AcordaKioskWPF.ViewModel
{
    public class ContentTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (!(item is ContentPageViewModel)) return base.SelectTemplate(item, container);

            var page_vm = (ContentPageViewModel) item;
            var found_template = Application.Current.MainWindow.FindResource(page_vm.PageName) as DataTemplate;
            return found_template;
        }
    }
}
