﻿using System.Linq;
using AcordaKioskWPF.Messages;
using AcordaKioskWPF.Model;
using AcordaKioskWPF.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace AcordaKioskWPF.ViewModel
{
    public class NameConfirmViewModel : ViewModelBase
    {
        public const int OTHER_HCP_TYPE_AID = 4;
        public const int QUESTION_1_ID = 0;

        private RelayCommand _confirmCommand;
        public RelayCommand ConfirmCommand
        {
            get
            {
                return _confirmCommand ?? (_confirmCommand = new RelayCommand(() =>
                {
                    var hcp_type = _svc.GetUserAnswer(_currentUser.BadgeNum, QUESTION_1_ID);
                    var user_type = hcp_type != null
                        ? hcp_type.SelectedAnswers.First()
                        : KioskSurveyQuestion.HcpType.AnswerMapping.First(a => a.Value == OTHER_HCP_TYPE_AID).Key;

                    _svc.SetUserType(user_type);

                    Messenger.Default.Send(new NextPageMessage(Pages.NameConfirm));  // always do survey for now
                    Messenger.Default.Send(new NameConfirmedMessage { User =  _currentUser });

                }));
            }
        }

        private RelayCommand _denyCommand;
        public RelayCommand DenyCommand
        {
            get
            {
                return _denyCommand ?? (_denyCommand = new RelayCommand(() =>
                {
                    Messenger.Default.Send(new NextPageMessage(Pages.NameDeny));
                }));
            }
        }

        private string _selectedName = "[Name]";
        public string SelectedName
        {
            get { return _selectedName; }
            set
            {
                if (_selectedName == value) return;
                _selectedName = value;
                RaisePropertyChanged();
            }
        }

        private Visitor _currentUser;
        private readonly IDataService _svc;

        public override void Cleanup()
        {
            base.Cleanup();
            Messenger.Default.Register<NameSelectedMessage>(this, msg =>
            {
                SelectedName = msg.User.FirstName + ",";
                _currentUser = msg.User;
            });
        }

        public NameConfirmViewModel(IDataService dataService)
        {
            _svc = dataService;
            Messenger.Default.Register<NameSelectedMessage>(this, msg =>
            {
                SelectedName = msg.User.FirstName + ",";
                _currentUser = msg.User;
            });
        }
    }
}