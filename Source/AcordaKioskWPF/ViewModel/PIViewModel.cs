﻿using System.Collections.ObjectModel;
using System.Linq;
using AcordaKioskWPF.Messages;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace AcordaKioskWPF.ViewModel
{
    public class PIViewModel : FooterPageViewModel
    {
        private int _selectedIndex;
        private static readonly string[] ImgPaths =
        {
            "pack://siteoforigin:,,,/Resources/imgPI_pg1.png",
            "pack://siteoforigin:,,,/Resources/imgPI_pg2.png",
            "pack://siteoforigin:,,,/Resources/imgPI_pg3.png",
            "pack://siteoforigin:,,,/Resources/imgPI_pg4.png",
            "pack://siteoforigin:,,,/Resources/imgPI_pg5.png",
            "pack://siteoforigin:,,,/Resources/imgPI_pg6.png",
            "pack://siteoforigin:,,,/Resources/imgPI_pg7.png",
            "pack://siteoforigin:,,,/Resources/imgPI_pg8.png",
            "pack://siteoforigin:,,,/Resources/imgPI_pg9.png"
        };

        private string _imgSrc = ImgPaths[0];
        public string ImageSource
        {
            get { return _imgSrc; }
            set
            {
                if (_imgSrc == value) return;
                _imgSrc = value;
                RaisePropertyChanged();
            }
        }

        private bool _prevEnabled;
        public bool PreviousButtonEnabled
        {
            get { return _prevEnabled; }
            set
            {
                if (_prevEnabled == value) return;
                _prevEnabled = value;
                RaisePropertyChanged();
            }
        }

        private bool _nextEnabled = true;
        public bool NextButtonEnabled
        {
            get { return _nextEnabled; }
            set
            {
                if (_nextEnabled == value) return;
                _nextEnabled = value;
                RaisePropertyChanged();
            }
        }

        private RelayCommand _nextPageCommand;
        public new RelayCommand NextPageCommand
        {
            get
            {
                return _nextPageCommand ?? (_nextPageCommand = new RelayCommand(() =>
                {
                    _selectedIndex++;
                    ImageSource = ImgPaths[_selectedIndex];
                    NextButtonEnabled = _selectedIndex < ImgPaths.Length - 1;
                    PreviousButtonEnabled = true;
                }));
            }
        }

        private RelayCommand _previousPageCommand;
        public RelayCommand PreviousPageCommand
        {
            get
            {
                return _previousPageCommand ?? (_previousPageCommand = new RelayCommand(() =>
                {
                    _selectedIndex--;
                    ImageSource = ImgPaths[_selectedIndex];
                    PreviousButtonEnabled = _selectedIndex > 0;
                    NextButtonEnabled = true;
                }));
            }
        }

        public PIViewModel()
        {
            Messenger.Default.Register<PopFooterMessage>(this, msg =>
            {
                _selectedIndex = 0;
                ImageSource = ImgPaths[_selectedIndex];
                PreviousButtonEnabled = false;
                NextButtonEnabled = true;
            });
        }
    }
}