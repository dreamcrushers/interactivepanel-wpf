﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace AcordaKioskWPF.Utils
{
    public static class ItemsControlExtensions
    {
        public static void AnimateScrollIntoView(this ItemsControl itemsControl, object item, bool isNext = true)
        {
            var scroll_viewer = VisualTreeHelpers.GetVisualChild<ScrollViewer>(itemsControl);
            if (scroll_viewer == null) return;

            double to_value;
            const double duration = 250;
            
            EasingMode easingMode = EasingMode.EaseOut;
            var from_value = scroll_viewer.HorizontalOffset;
            var container = (Control) itemsControl.ItemContainerGenerator.ContainerFromItem(item);
            if (container == null) return;
            var index = itemsControl.ItemContainerGenerator.IndexFromContainer(container);
            to_value = scroll_viewer.ScrollableWidth * ((double)index / (itemsControl.Items.Count - 1));

            if (from_value == to_value)
            {
                if(isNext)
                {
                    from_value -= container.ActualWidth;
                }
                else
                {
                    from_value += container.ActualWidth;
                }
            }          

            var animation = new DoubleAnimation
            {
                From = from_value,
                To = to_value,
                Duration = new Duration(TimeSpan.FromMilliseconds(duration)),
                EasingFunction = new CubicEase
                {
                    EasingMode = easingMode
                }
            };

            var sb = new Storyboard();

            sb.Children.Add(animation);
            Storyboard.SetTarget(animation, scroll_viewer);
            Storyboard.SetTargetProperty(animation, new PropertyPath(AnimatedHorizScrollBehavior.HorizontalOffsetProperty));
            sb.Begin();
        }
    }
}
