﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Input;

namespace AcordaKioskWPF.View
{

	public partial class DebugConsole
	{
	    public static DebugConsole Current { get; } = new DebugConsole();

	    public DebugConsole()
		{
			InitializeComponent();
			KeyDown += DebugConsole_KeyDown;
			Closing += DebugConsole_Closing;

			var main_window = Application.Current.MainWindow;
			if (main_window != null)
			{
				if (ReferenceEquals(main_window, this))
				{
					Console.Error.WriteLine("Debug Console must be instantiated after the main window.");
				}
				else
				{
					main_window.AddHandler(Keyboard.KeyDownEvent, new KeyEventHandler(MainWindow_KeyDown), true);
					Owner = main_window;
				}
			}

			Width = 1024;
			Height = 768;

			var entry_name = Assembly.GetEntryAssembly().GetName();
	        Title = $"{entry_name.Name} version: {entry_name.Version}";
		}

		public void ToggleVisibility()
		{
			Visibility = Visibility == Visibility.Visible ? Visibility.Hidden : Visibility.Visible;
		}

		private void DebugConsole_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.OemTilde)
			{
				ToggleVisibility();
			}

			e.Handled = true;
		}

		private void DebugConsole_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// Don't close the window, just hide it
			e.Cancel = true;
			Visibility = Visibility.Hidden;
		}

		private void MainWindow_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.OemTilde)
			{
				ToggleVisibility();
			}
		}
	}
}
