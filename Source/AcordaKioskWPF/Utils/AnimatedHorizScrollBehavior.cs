﻿using System.Windows;
using System.Windows.Controls;

namespace AcordaKioskWPF.Utils
{
    public class AnimatedHorizScrollBehavior
    {
        public static DependencyProperty HorizontalOffsetProperty =
        DependencyProperty.RegisterAttached("HorizontalOffset",
                                            typeof(double),
                                            typeof(AnimatedHorizScrollBehavior),
                                            new UIPropertyMetadata(0.0, OnHorizontalOffsetChanged));

        public static void SetHorizontalOffset(FrameworkElement target, double value)
        {
            target.SetValue(HorizontalOffsetProperty, value);
        }

        public static double GetHorizontalOffset(FrameworkElement target)
        {
            return (double)target.GetValue(HorizontalOffsetProperty);
        }

        private static void OnHorizontalOffsetChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            var scroll_viewer = target as ScrollViewer;
            scroll_viewer?.ScrollToHorizontalOffset((double)e.NewValue);
        }
    }
}
