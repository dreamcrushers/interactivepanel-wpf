﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using AcordaKioskWPF.Messages;
using AcordaKioskWPF.Model;
using AcordaKioskWPF.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using AcordaKioskWPF.Properties;

namespace AcordaKioskWPF.ViewModel
{
    public class SelectLimitViewModel : ViewModelBase
    {
        private bool _isLoading;

        private RelayCommand _loadedCommand;
        public RelayCommand LoadedCommand => _loadedCommand ?? (_loadedCommand = new RelayCommand(() =>
        {
            SetQuestionTextFromType();

            _isLoading = true;

            var selected = _limits.FirstOrDefault(n => n.IsSelected);
            if (selected != null) selected.IsSelected = false;

            _isLoading = false;

            // reset any selected values from this session
            // this works because we have the same question id for all the limit questions
            var answer = _dataService.GetCachedAnswer(KioskSurveyQuestion.LimitPWP.QuestionId);
            if (answer == null || answer.SelectedAnswers.Count == 0) return;
            foreach (var selected_answer in answer.SelectedAnswers)
            {
                var spec = _limits.FirstOrDefault(s => s.Value == selected_answer);
                if (spec != null) spec.IsSelected = true;
            }
        }));

        private string _questionText;
        public string QuestionText
        {
            get { return _questionText; }
            set
            {
                if (_questionText == value) return;
                _questionText = value;
                RaisePropertyChanged();
            }
        }

        private string _questionNumberText;
        public string QuestionNumberText
        {
            get { return _questionNumberText; }
            set
            {
                if (_questionNumberText == value) return;
                _questionNumberText = value;
                RaisePropertyChanged();
            }
        }

        public const int DONT_HAVE_AID = 12;
        public const int DONT_KNOW_AID = 13;

        private ObservableCollection<SelectedObject> _limits;
        public ObservableCollection<SelectedObject> Limits
        {
            get
            {
                if (_limits != null) return _limits;
                _limits = GetLimitsFromHcpType();
                return _limits;
            }
            set
            {
                _limits = value;
                RaisePropertyChanged();
            }
        }

        private ObservableCollection<SelectedObject> GetLimitsFromHcpType(int type = -1)
        {
            switch (type)
            {
                case UserType.PWP:
                    return new ObservableCollection<SelectedObject>(
                        KioskSurveyQuestion.LimitPWP.AnswerMapping.Select(k => new SelectedObject(k.Key, LimitPropertyChanged)).ToArray()
                    );
                case UserType.CAREGIVER:
                    return new ObservableCollection<SelectedObject>(
                        KioskSurveyQuestion.LimitCarePartner.AnswerMapping.Select(k => new SelectedObject(k.Key, LimitPropertyChanged)).ToArray()
                    );
                case UserType.HCP:
                    return new ObservableCollection<SelectedObject>(
                        KioskSurveyQuestion.LimitHCP.AnswerMapping.Select(k => new SelectedObject(k.Key, LimitPropertyChanged)).ToArray()
                    );
                case UserType.OTHER:
                    return new ObservableCollection<SelectedObject>(
                        KioskSurveyQuestion.LimitOther.AnswerMapping.Select(k => new SelectedObject(k.Key, LimitPropertyChanged)).ToArray()
                    );
                default:
                    return new ObservableCollection<SelectedObject>(
                        KioskSurveyQuestion.LimitPWP.AnswerMapping.Select(k => new SelectedObject(k.Key, LimitPropertyChanged)).ToArray()
                    );
            }
        }

        // ensure only one name can be selected at once
        private void LimitPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "IsSelected" || _isLoading) return;

            Messenger.Default.Send(new SurveyQuestionSelectedMessage());

            var answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.LimitPWP);

            var type = UserType.IdFromType(_dataService.GetUserType());
            switch (type)
            {
                case UserType.PWP:
                    answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.LimitPWP);
                    break;
                case UserType.CAREGIVER:
                    answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.LimitCarePartner);
                    break;
                case UserType.HCP:
                    answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.LimitHCP);
                    break;
                case UserType.OTHER:
                    answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.LimitOther);
                    break;
            }

            answer.SelectedAnswers = _limits.Where(o => o.IsSelected).Select(o => o.Value).ToList();
            _dataService.CacheSurveyAnswer(answer);
        }

        public void SetQuestionTextFromType()
        {
            var text = "";
            var type = UserType.IdFromType(_dataService.GetUserType());
            switch (type)
            {
                case UserType.PWP:
                    text += Settings.Default.Question5_PersonalWithParkinsons;
                    QuestionNumberText = "#5 of 5";
                    break;
                case UserType.CAREGIVER:
                    text += Settings.Default.Question5_CarePartner;
                    QuestionNumberText = "#5 of 5";
                    break;
                case UserType.HCP:
                    text += Settings.Default.Question5_HealthcareProvider;
                    QuestionNumberText = "#5 of 5";
                    break;
                case UserType.OTHER:
                    text += Settings.Default.Question5_Other;
                    QuestionNumberText = "#5 of 5";
                    break;
            }
            QuestionText = text;
            Limits = GetLimitsFromHcpType(type);
            _currentHcpType = type;
        }

        private int _currentHcpType;
        private readonly IDataService _dataService;
        public SelectLimitViewModel(IDataService dataService)
        {
            _dataService = dataService;
        }
    }
}
