﻿namespace AcordaKioskWPF.Messages
{
    public class SurveyFinishedMessage : UserMessage
    {
        public string TalkToPatients { get; set; }
        public string WalkingDifficulty { get; set; }
        public string WhenDiscuss { get; set; }
        public string HowManyPatients { get; set; }
        public string WhatPercentage { get; set; }
    }
}
