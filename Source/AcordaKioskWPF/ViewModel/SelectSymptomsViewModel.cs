﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using AcordaKioskWPF.Messages;
using AcordaKioskWPF.Model;
using AcordaKioskWPF.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using AcordaKioskWPF.Properties;

namespace AcordaKioskWPF.ViewModel
{
    public class SelectSymptomsViewModel : ViewModelBase
    {
        private bool _isLoading;

        private RelayCommand _loadedCommand;
        public RelayCommand LoadedCommand => _loadedCommand ?? (_loadedCommand = new RelayCommand(() =>
        {
            SetQuestionTextFromType();

            _isLoading = true;

            var selected = _symptoms.FirstOrDefault(n => n.IsSelected);
            if (selected != null) selected.IsSelected = false;

            _isLoading = false;

            // reset any selected values from this session
            var answer = _dataService.GetCachedAnswer(KioskSurveyQuestion.Symptom.QuestionId);
            if (answer == null || answer.SelectedAnswers.Count == 0) return;
            foreach (var selected_answer in answer.SelectedAnswers)
            {
                var spec = _symptoms.FirstOrDefault(s => s.Value == selected_answer);
                if (spec != null) spec.IsSelected = true;
            }
        }));

        private string _questionText;
        public string QuestionText
        {
            get { return _questionText; }
            set
            {
                if (_questionText == value) return;
                _questionText = value;
                RaisePropertyChanged();
            }
        }

        private string _questionNumberText;
        public string QuestionNumberText
        {
            get { return _questionNumberText; }
            set
            {
                if (_questionNumberText == value) return;
                _questionNumberText = value;
                RaisePropertyChanged();
            }
        }

        public const int DONT_HAVE_AID = 12;
        public const int DONT_KNOW_AID = 13;

        private ObservableCollection<SelectedObject> _symptoms;
        public ObservableCollection<SelectedObject> Symptoms
        {
            get
            {
                if (_symptoms != null) return _symptoms;
                _symptoms = GetSymptomsFromHcpType();
                return _symptoms;
            }
            set
            {
                _symptoms = value;
                RaisePropertyChanged();
            }
        }

        private ObservableCollection<SelectedObject> GetSymptomsFromHcpType(int type = -1)
        {
            var mapping = KioskSurveyQuestion.Symptom.AnswerMapping;
            switch (type) {
                case UserType.HCP:
                    return new ObservableCollection<SelectedObject>(
                        mapping
                            .Where(m => m.Value != DONT_KNOW_AID)
                            .Select(k => new SelectedObject(k.Key, SpecialtyPropertyChanged)).ToArray()
                        );
                case UserType.OTHER:
                    return new ObservableCollection<SelectedObject>(
                        mapping
                            .Where(m => m.Value != DONT_HAVE_AID)
                            .Select(k => new SelectedObject(k.Key, SpecialtyPropertyChanged)).ToArray()
                        );
                default:
                    return new ObservableCollection<SelectedObject>(
                        mapping
                            .Where(m => m.Value != DONT_HAVE_AID && m.Value != DONT_KNOW_AID)
                            .Select(k => new SelectedObject(k.Key, SpecialtyPropertyChanged)).ToArray()
                        );
            }
        }

        // ensure only one name can be selected at once
        private void SpecialtyPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "IsSelected" || _isLoading) return;

            UpdateSelectedAnswers(sender);

            var answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.Symptom);

            var type = UserType.IdFromType(_dataService.GetUserType());
            switch (type)
            {
                case UserType.PWP:
                    answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.SymptomPersonWithParkinsons);
                    break;
                case UserType.CAREGIVER:
                    answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.SymptomCarePartner);
                    break;
                case UserType.HCP:
                    answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.SymptomHeathcareProvider);
                    break;
                case UserType.OTHER:
                    answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.SymptomOther);
                    break;
            }

            answer.SelectedAnswers = _symptoms.Where(o => o.IsSelected).Select(o => o.Value).ToList();

            _dataService.CacheSurveyAnswer(answer);

            Messenger.Default.Send(new SurveyQuestionSelectedMessage
            {
                NextPage = answer.SelectedAnswers.Contains("They don't have OFF periods") ? Pages.FinishSurvey : ""
            });
        }

        private void UpdateSelectedAnswers(object sender)
        {
            if (_symptoms.FirstOrDefault(s => s.Value == Settings.Default.DefaultNoneString).IsSelected && (sender as SelectedObject).Value == Settings.Default.DefaultNoneString)
            {
                foreach (var symptomAnswer in _symptoms)
                {
                    if (symptomAnswer.Value != Settings.Default.DefaultNoneString)
                    {
                        symptomAnswer.IsSelected = false;
                    }
                }
            }
            else
            {
                var selectedAnswer = _symptoms.Where(o => o.IsSelected).Select(o => o.Value).ToList();
                var selectedObject = (SelectedObject)sender;

                if (selectedObject.Value != Settings.Default.DefaultNoneString && !(sender as SelectedObject).IsSelected)
                {
                    if(selectedObject.Value == Settings.Default.DefaultNoneString && selectedAnswer.Count() <= 1)
                    { 
                        _symptoms.FirstOrDefault(s => s.Value == Settings.Default.DefaultNoneString).IsSelected = true;
                    }
                }
                else
                {
                    _symptoms.FirstOrDefault(s => s.Value == Settings.Default.DefaultNoneString).IsSelected = false;
                }
            }
        }

        public void SetQuestionTextFromType()
        {
            var text = "";
            var type = UserType.IdFromType(_dataService.GetUserType());
            switch (type)
            {
                case UserType.PWP:
                    text += Settings.Default.Question2_PersonalWithParkinsons;
                    QuestionNumberText = $"#2 of 5";
                    break;
                case UserType.CAREGIVER:
                    text += Settings.Default.Question2_CarePartner;
                    QuestionNumberText = $"#2 of 5";
                    break;
                case UserType.HCP:
                    text += Settings.Default.Question2_HealthcareProvider;
                    QuestionNumberText = $"#2 of 5";
                    break;
                case UserType.OTHER:
                    text += Settings.Default.Question2_Other;
                    QuestionNumberText = $"#2 of 5";
                    break;
            }
            QuestionText = text;
            Symptoms = GetSymptomsFromHcpType(type);
        }

        private readonly IDataService _dataService;

        public SelectSymptomsViewModel(IDataService dataService)
        {
            _dataService = dataService;
        }
    }
}
