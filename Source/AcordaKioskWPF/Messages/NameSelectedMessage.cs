﻿using AcordaKioskWPF.Model;

namespace AcordaKioskWPF.Messages
{
    public class NameSelectedMessage : UserMessage
    {
        public string Name { get; set; }
        public string UserType { get; set; }
    }

    public class NameSelectedPreMessage
    {
        
    }
}
