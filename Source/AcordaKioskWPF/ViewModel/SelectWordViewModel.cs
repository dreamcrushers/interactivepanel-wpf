﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using AcordaKioskWPF.Messages;
using AcordaKioskWPF.Model;
using AcordaKioskWPF.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace AcordaKioskWPF.ViewModel
{
    public class SelectWordViewModel : ViewModelBase
    {
        private bool _isLoading;

        private RelayCommand _loadedCommand;
        public RelayCommand LoadedCommand
        {
            get
            {
                return _loadedCommand ?? (_loadedCommand = new RelayCommand(() =>
                {
                    _isLoading = true;

                    SetQuestionTextFromType();

                    // clear old selected from old sessions
                    var selected = _words.FirstOrDefault(n => n.IsSelected);
                    if (selected != null) selected.IsSelected = false;

                    _isLoading = false;

                    // reset any selected values from this session
                    var answer = _dataService.GetCachedAnswer(KioskSurveyQuestion.Word.QuestionId);
                    if (answer == null || answer.SelectedAnswers.Count == 0) return;
                    foreach (var selected_answer in answer.SelectedAnswers)
                    {
                        var spec = _words.FirstOrDefault(s => s.Value == selected_answer);
                        if (spec != null) spec.IsSelected = true;
                    }
                }));
            }
        }

        public void SetQuestionTextFromType()
        {
            switch (UserType.IdFromType(_dataService.GetUserType()))
            {
                case UserType.PWP:
                    QuestionNumberText = $"#4 of 5";
                    break;
                case UserType.CAREGIVER:
                    QuestionNumberText = $"#4 of 5";
                    break;
                case UserType.HCP:
                    QuestionNumberText = $"#4 of 5";
                    break;
                case UserType.OTHER:
                    QuestionNumberText = $"#4 of 5";
                    break;
            }
        }

        private ObservableCollection<SelectedObject> _words;
        public ObservableCollection<SelectedObject> Words
        {
            get
            {
                if (_words != null) return _words;
                _words = new ObservableCollection<SelectedObject>(
                    KioskSurveyQuestion.Word.AnswerMapping.Select(k => new SelectedObject(k.Key, WordPropertyChanged)).ToArray()
                );
                return _words;
            }
            set
            {
                _words = value;
                RaisePropertyChanged();
            }
        }

        private string _questionNumberText;
        public string QuestionNumberText
        {
            get { return _questionNumberText; }
            set
            {
                if (_questionNumberText == value) return;
                _questionNumberText = value;
                RaisePropertyChanged();
            }
        }

        // ensure only one name can be selected at once
        private void WordPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "IsSelected" || _isLoading) return;

            Messenger.Default.Send(new SurveyQuestionSelectedMessage());

            // if this is the thing we've selected, go ahead and set the question and clear the olds
            // if we've deselected all the items, clear the answer
            // if we've selected something else, this is just an event fired killing the old selection, so ignore
            var s = (SelectedObject)sender;
            if (s.IsSelected)
            {
                var answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.Word);
                answer.SelectedAnswers.Add(s.Value);
                _dataService.CacheSurveyAnswer(answer);

                foreach (var spec in Words)
                {
                    if (s.Value != spec.Value) spec.IsSelected = false;
                }
            }
            else if (!_words.Any(o => o.IsSelected))
            {
                var answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.Word);
                _dataService.CacheSurveyAnswer(answer);
            }
        }

        private readonly IDataService _dataService;
        public SelectWordViewModel(IDataService dataService)
        {
            _dataService = dataService;
        }
    }
}
