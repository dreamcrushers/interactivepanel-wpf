﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using AcordaKioskWPF.Messages;
using AcordaKioskWPF.Model;
using AcordaKioskWPF.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using AcordaKioskWPF.Properties;

namespace AcordaKioskWPF.ViewModel
{
    public class SelectFeelingsViewModel : ViewModelBase
    {
        private bool _isLoading;

        private RelayCommand _loadedCommand;
        public RelayCommand LoadedCommand => _loadedCommand ?? (_loadedCommand = new RelayCommand(() =>
        {
            SetQuestionTextFromType();

            _isLoading = true;

            var selected = _feelings.FirstOrDefault(n => n.IsSelected);
            if (selected != null) selected.IsSelected = false;

            _isLoading = false;

            // reset any selected values from this session
            var answer = _dataService.GetCachedAnswer(KioskSurveyQuestion.Feeling.QuestionId);
            if (answer == null || answer.SelectedAnswers.Count == 0) return;
            foreach (var selected_answer in answer.SelectedAnswers)
            {
                var spec = _feelings.FirstOrDefault(s => s.Value == selected_answer);
                if (spec != null) spec.IsSelected = true;
            }
        }));

        private string _questionText;
        public string QuestionText
        {
            get { return _questionText; }
            set
            {
                if (_questionText == value) return;
                _questionText = value;
                RaisePropertyChanged();
            }
        }

        private string _questionNumberText;
        public string QuestionNumberText
        {
            get { return _questionNumberText; }
            set
            {
                if (_questionNumberText == value) return;
                _questionNumberText = value;
                RaisePropertyChanged();
            }
        }

        public const int DONT_REPORT_AID = 14;
        public const int DONT_KNOW_AID = 15;

        private ObservableCollection<SelectedObject> _feelings;
        public ObservableCollection<SelectedObject> Feelings
        {
            get
            {
                if (_feelings != null) return _feelings;
                _feelings = GetFeelingsFromHcpType();
                return _feelings;
            }
            set
            {
                _feelings = value;
                RaisePropertyChanged();
            }
        }

        private ObservableCollection<SelectedObject> GetFeelingsFromHcpType(int type = -1)
        {
            var mapping = KioskSurveyQuestion.Feeling.AnswerMapping;

            switch (type)
            {
                case UserType.HCP:
                    return new ObservableCollection<SelectedObject>(
                        mapping
                            .Where(m => m.Value != DONT_REPORT_AID)
                            .Where(m => m.Value != DONT_KNOW_AID)
                            .Select(k => new SelectedObject(k.Key, SpecialtyPropertyChanged)).ToArray()

                    );
                case UserType.OTHER:
                    return new ObservableCollection<SelectedObject>(
                        mapping
                            .Where(m => m.Value != DONT_REPORT_AID)
                            .Select(k => new SelectedObject(k.Key, SpecialtyPropertyChanged)).ToArray()
                    );
                default:
                    return new ObservableCollection<SelectedObject>(
                        mapping
                            .Where(m => m.Value != DONT_REPORT_AID)
                            .Where(m => m.Value != DONT_KNOW_AID)
                            .Select(k => new SelectedObject(k.Key, SpecialtyPropertyChanged)).ToArray()
                        );
            }
        }

        // ensure only one name can be selected at once
        private void SpecialtyPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "IsSelected" || _isLoading) return;

            Messenger.Default.Send(new SurveyQuestionSelectedMessage());

            UpdateSelectedAnswers(sender);

            var answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.Feeling);

            var type = UserType.IdFromType(_dataService.GetUserType());
            switch (type)
            {
                case UserType.PWP:
                    answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.FeelingPersonWithParkinsons);
                    break;
                case UserType.CAREGIVER:
                    answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.FeelingCarePartner);
                    break;
                case UserType.HCP:
                    answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.FeelingHealthcareProvider);
                    break;
                case UserType.OTHER:
                    answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.FeelingOther);
                    break;
            }


            answer.SelectedAnswers = _feelings.Where(o => o.IsSelected).Select(o => o.Value).ToList();
            _dataService.CacheSurveyAnswer(answer);
        }

        private void UpdateSelectedAnswers(object sender)
        {
            if (_feelings.FirstOrDefault(s => s.Value == Settings.Default.DefaultNoneString).IsSelected && (sender as SelectedObject).Value == Settings.Default.DefaultNoneString)
            {
                foreach (var symptomAnswer in _feelings)
                {
                    if (symptomAnswer.Value != Settings.Default.DefaultNoneString)
                    {
                        symptomAnswer.IsSelected = false;
                    }
                }
            }
            else
            {
                var selectedAnswer = _feelings.Where(o => o.IsSelected).Select(o => o.Value).ToList();

                var selectedObject = (SelectedObject)sender;
                if (selectedObject.Value != Settings.Default.DefaultNoneString && !(sender as SelectedObject).IsSelected)
                {
                    if (selectedObject.Value == Settings.Default.DefaultNoneString && selectedAnswer.Count() <= 1)
                    { 
                        _feelings.FirstOrDefault(s => s.Value == Settings.Default.DefaultNoneString).IsSelected = true;
                    }
                }
                else
                {
                    _feelings.FirstOrDefault(s => s.Value == Settings.Default.DefaultNoneString).IsSelected = false;
                }
            }
        }

        public void SetQuestionTextFromType()
        {
            var text = "";
            var type = UserType.IdFromType(_dataService.GetUserType());
            switch (type)
            {
                case UserType.PWP:
                    text += Settings.Default.Question3_PersonalWithParkinsons;
                    QuestionNumberText = $"#3 of 5";
                    break;
                case UserType.CAREGIVER:
                    text += Settings.Default.Question3_CarePartner;
                    QuestionNumberText = $"#3 of 5";
                    break;
                case UserType.HCP:
                    text += Settings.Default.Question3_HealthcareProvider;
                    QuestionNumberText = $"#3 of 5";
                    break;
                case UserType.OTHER:
                    text += Settings.Default.Question3_Other;
                    QuestionNumberText = $"#3 of 5";
                    break;
            }
            QuestionText = text;
            Feelings = GetFeelingsFromHcpType(type);
        }

        private readonly IDataService _dataService;
        public SelectFeelingsViewModel(IDataService dataService)
        {
            _dataService = dataService;
        }
    }
}
