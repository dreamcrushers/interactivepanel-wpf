﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using AcordaKioskWPF.Messages;
using AcordaKioskWPF.Model;
using AcordaKioskWPF.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace AcordaKioskWPF.ViewModel
{
    public class SelectSpecialtyViewModel : ViewModelBase
    {
        private bool _isLoading;

        private RelayCommand _loadedCommand;
        public RelayCommand LoadedCommand
        {
            get
            {
                return _loadedCommand ?? (_loadedCommand = new RelayCommand(() =>
                {
                    _isLoading = true;

                    // clear old selected from old sessions
                    var selected = _specialties.FirstOrDefault(n => n.IsSelected);
                    if (selected != null) selected.IsSelected = false;

                    _isLoading = false;

                    // reset any selected values from this session
                    var answer = _dataService.GetCachedAnswer(KioskSurveyQuestion.Specialty.QuestionId);
                    if (answer == null || answer.SelectedAnswers.Count == 0) return;
                    foreach (var selected_answer in answer.SelectedAnswers)
                    {
                        var spec = _specialties.FirstOrDefault(s => s.Value == selected_answer);
                        if (spec != null) spec.IsSelected = true;
                    }
                }));
            }
        }

        private ObservableCollection<SelectedObject> _specialties;
        public ObservableCollection<SelectedObject> Specialties
        {
            get
            {
                if (_specialties != null) return _specialties;
                _specialties = new ObservableCollection<SelectedObject>(
                    KioskSurveyQuestion
                        .Specialty
                        .AnswerMapping
                        .Select(k => new SelectedObject(k.Key, SpecialtyPropertyChanged)).ToArray()
                    );
                return _specialties;
            }
            set
            {
                _specialties = value;
                RaisePropertyChanged();
            }
        }

        private string _questionNumberText;
        public string QuestionNumberText
        {
            get { return _questionNumberText; }
            set
            {
                if (_questionNumberText == value) return;
                _questionNumberText = value;
                RaisePropertyChanged();
            }
        }

        // ensure only one name can be selected at once
        private void SpecialtyPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "IsSelected" || _isLoading) return;

            Messenger.Default.Send(new SurveyQuestionSelectedMessage());

            // if this is the thing we've selected, go ahead and set the question and clear the olds
            // if we've deselected all the items, clear the answer
            // if we've selected something else, this is just an event fired killing the old selection, so ignore
            var s = (SelectedObject)sender;
            if (s.IsSelected)
            {
                var answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.Specialty);
                answer.SelectedAnswers.Add(s.Value);
                _dataService.CacheSurveyAnswer(answer);

                foreach (var spec in Specialties)
                {
                    if (s.Value != spec.Value) spec.IsSelected = false;
                }
            }else if (!_specialties.Any(o => o.IsSelected))
            {
                var answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.Specialty);
                _dataService.CacheSurveyAnswer(answer);
            }
        }

        private Visitor _currentUser;
        private readonly IDataService _dataService;
        public SelectSpecialtyViewModel(IDataService dataService)
        {
            _dataService = dataService;
            Messenger.Default.Register<NameConfirmedMessage>(this, msg =>
            {
                _currentUser = msg.User;
            });
        }
    }
}
