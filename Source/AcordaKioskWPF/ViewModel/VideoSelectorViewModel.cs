﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AcordaKioskWPF.Messages;
using AcordaKioskWPF.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System.Windows.Forms;
using AcordaKioskWPF.Properties;
using AcordaKioskWPF.Utils;

namespace AcordaKioskWPF.ViewModel
{
    public class VideoSelectorViewModel : ViewModelBase
    {
        private ObservableCollection<VideoSelection> _videoSelections;
        public ObservableCollection<VideoSelection> VideoSelections
        {
            get { return _videoSelections; }
            set
            {
                if (_videoSelections == value) return;
                _videoSelections = value;
                RaisePropertyChanged();
            }
        }

        private VideoSelection _selection;
        public VideoSelection Selection
        {
            get { return _selection; }
            set
            {
                if (_selection == value) return;
                _selection = value;
                RaisePropertyChanged();
            }
        }

        private RelayCommand _nextVideoCommand;
        public RelayCommand NextVideoCommand
        {
            get
            {
                return _nextVideoCommand ?? (_nextVideoCommand = new RelayCommand(() =>
                {
                    var current_idx = VideoSelections.IndexOf(Selection);
                 //   if (current_idx >= VideoSelections.Count - 1) return;

                    UpdateVideoIndex(current_idx + 1, true);

                    _previousPatientName = Selection.PatientName;
                }));
            }
        }

        private RelayCommand _nextVideoSwipeCommand;
        public RelayCommand NextVideoSwipeCommand
        {
            get
            {
                return _nextVideoSwipeCommand ?? (_nextVideoSwipeCommand = new RelayCommand(() =>
                {
                    _previousPatientName = Selection.PatientName;

                    var current_idx = VideoSelections.IndexOf(Selection);
                    //if (current_idx >= VideoSelections.Count - 1) return;

                    UpdateVideoIndex(current_idx + 1, true);
                }));
            }
        }

        private RelayCommand _previousVideoCommand;
        public RelayCommand PreviousVideoCommand
        {
            get
            {
                return _previousVideoCommand ?? (_previousVideoCommand = new RelayCommand(() =>
                {
                    var current_idx = VideoSelections.IndexOf(Selection);
                 //   if (current_idx == 0) return;

                    UpdateVideoIndex(current_idx - 1, false);

                    _previousPatientName = Selection.PatientName;
                }));
            }
        }

        private RelayCommand _previousVideoSwipeCommand;
        public RelayCommand PreviousVideoSwipeCommand
        {
            get
            {
                return _previousVideoSwipeCommand ?? (_previousVideoSwipeCommand = new RelayCommand(() =>
                {
                    _previousPatientName = Selection.PatientName;

                    var current_idx = VideoSelections.IndexOf(Selection);
                    //if (current_idx == 0) return;

                    UpdateVideoIndex(current_idx - 1, false);
                }));
            }
        }

        public void UpdateVideoIndex(int idx, bool next)
        { 

            if (next)
            {
                if (idx >= VideoSelections.Count - 1)
                {
                    var temp = VideoSelections[0];
                    VideoSelections.Remove(temp);
                    VideoSelections.Add(temp);
                    idx--;

                    if (idx + 1 >= VideoSelections.Count)
                    {
                        idx -= (VideoSelections.Count - idx);
                    }
                }
            }
            else
            {
                if (idx <= 1)
                {
                    var temp = VideoSelections[VideoSelections.Count - 1];
                    VideoSelections.Remove(temp);
                    VideoSelections.Insert(0, temp);
                    idx++;

                    if (idx + 1 >= VideoSelections.Count)
                    {
                        idx -= (VideoSelections.Count - idx);
                    }
                }
            }

            //If there are 3 or more videos then we can display both PREV/NEXT bttons
            //In case there are only two videos then only next button is displayed
            //If there is only 1 video then both NEXT/PREV buttons are hidden
            if (VideoSelections.Count >= 3)
            {
                var prevIdx = idx;
                var nextIdx = idx;

                if (idx == 0)
                {
                    prevIdx = 8;
                }
                if (idx == 8)
                {
                    nextIdx = 0;
                }


                PrevButtonLabel = VideoSelections[prevIdx - 1].PatientName;
                NextButtonLabel = VideoSelections[nextIdx + 1].PatientName;
                NextButtonEnabled = true;
                PrevButtonEnabled = true;
            }
            else
            {
                idx = 0;
                NextButtonEnabled = false;
                PrevButtonEnabled = false;
            }

            Selection = VideoSelections[idx];
            StoryLabel = $"Play {Selection.PatientName}'s Story.";
        }

        private string _nextBtnLabel;
        public string NextButtonLabel
        {
            get { return _nextBtnLabel; }
            set
            {
                if (_nextBtnLabel == value) return;
                _nextBtnLabel = value;
                RaisePropertyChanged();
            }
        }

        private string _prevBtnLabel;
        public string PrevButtonLabel
        {
            get { return _prevBtnLabel; }
            set
            {
                if (_prevBtnLabel == value) return;
                _prevBtnLabel = value;
                RaisePropertyChanged();
            }
        }

        private bool _nextBtnEnabled;
        public bool NextButtonEnabled
        {
            get { return _nextBtnEnabled; }
            set
            {
                if (_nextBtnEnabled == value) return;
                _nextBtnEnabled = value;
                RaisePropertyChanged();
            }
        }

        private bool _prevBtnEnabled;
        public bool PrevButtonEnabled
        {
            get { return _prevBtnEnabled; }
            set
            {
                if (_prevBtnEnabled == value) return;
                _prevBtnEnabled = value;
                RaisePropertyChanged();
            }
        }

        private string _storyLabel;
        public string StoryLabel
        {
            get { return _storyLabel; }
            set
            {
                if (_storyLabel == value) return;
                _storyLabel = value;
                RaisePropertyChanged();
            }
        }

        private RelayCommand _playVideoCommand;
        public RelayCommand PlayVideoCommand
        {
            get
            {
                return _playVideoCommand ?? (_playVideoCommand = new RelayCommand(() =>
                {
                    if (!IsSwipe())
                    {
                        var selected_path = Selection.VideoPath;
                        _dataService.MarkPatientWatched(Selection.PatientName);
                        Messenger.Default.Send(new NextPageMessage(typeof(VideoSelectorViewModel)));    // this will cause cleanup, which will reset the selection to default
                        Messenger.Default.Send(new PlayVideoMessage { VideoPath = selected_path, VideoName = Selection.PatientName });
                    }

                    _previousPatientName = Selection.PatientName;
                }));
            }
        }

        private string _previousPatientName;
        private bool IsSwipe()
        {
            if (_previousPatientName != Selection.PatientName)
            {
                return true;
            }

            return false;
        }

        public override void Cleanup()
        {
            base.Cleanup();
            MarkPatientsWatched();
            UpdateDefaultVideoIndex(true);
        }

        private readonly IDataService _dataService;

        public VideoSelectorViewModel(IDataService dataService)
        {
            _dataService = dataService;
            VideoSelections = new ObservableCollection<VideoSelection>(VideoSelection.DefaultSelections);

            var patients = MarkPatientsWatched();
            var defaultVideoIndex = DefaultVideoSelector.GetDefaultVideoIndex();
            var continueLoop = true;
            var currentVideoIndex = defaultVideoIndex;

            while (continueLoop)
            {
                if (patients.Count() < VideoSelections.Count())
                {
                    if (patients.Contains(VideoSelections[currentVideoIndex].PatientName))
                    {
                        if (currentVideoIndex == 7)
                        {
                            currentVideoIndex = -1;
                        }

                        currentVideoIndex++;
                    }
                    else
                    {
                        continueLoop = false;
                    }
                }
                else
                {
                    continueLoop = false;
                }
            }


            var nextVideoIndex = currentVideoIndex > (VideoSelections.Count - 1) ? 0 : currentVideoIndex;

            UpdateVideoIndex(nextVideoIndex, true);


            //UpdateDefaultVideoIndex(true);

            _previousPatientName = Selection.PatientName;
        }

        private void UpdateDefaultVideoIndex(bool next)
        {
            UpdateVideoIndex(DefaultVideoSelector.GetDefaultVideoIndex(), next);
        }

        private string[] MarkPatientsWatched()
        {
            var patients = _dataService.GetWatchedPatients();
            foreach (var v in VideoSelections) {
                v.VideoWatched = patients.Contains(v.PatientName);
            }
            return patients;
        }
    }

    public class VideoSelection
    {
        public string Patient { get; set; }
        public string PatientName { get; set; }
        public string PullQuote { get; set; }
        public string BgUrl { get; set; }
        public string VideoPath { get; set; }
        public bool VideoWatched { get; set; }
        public string PatientQuoteImagePath { get; set; }
        
        public VideoSelection Copy()
        {
            return new VideoSelection
            {
                Patient = Patient,
                PatientName = PatientName,
                PullQuote = PullQuote,
                BgUrl = BgUrl,
                VideoPath = VideoPath,
                VideoWatched = VideoWatched,
                PatientQuoteImagePath = PatientQuoteImagePath
            };
        }

        public static VideoSelection[] DefaultSelections = {
            new VideoSelection
            {
                Patient = Pages.PatientMikeB,
                PatientName = "Mike B.",
                PullQuote = "“I feel disconnected with my mind and body.”",
                BgUrl = "pack://siteoforigin:,,,/Resources/Images/PatientVideos/bgMikeB.png",
                VideoPath = $"pack://siteoforigin:,,,/Resources/Videos/Story/{Settings.Default.StoryVideo_MikeB}",
                PatientQuoteImagePath = "pack://siteoforigin:,,,/Resources/Images/PatientQuotes/MikeBQuote.png"
            },
            new VideoSelection
            {
                Patient = Pages.PatientSteve,
                PatientName = "Steve",
                PullQuote = "“Just give me five more minutes, just give me five more minutes...”",
                BgUrl = "pack://siteoforigin:,,,/Resources/Images/PatientVideos/bgSteve.png",
                VideoPath = $"pack://siteoforigin:,,,/Resources/Videos/Story/{Settings.Default.StoryVideo_Steve}",
                PatientQuoteImagePath = "pack://siteoforigin:,,,/Resources/Images/PatientQuotes/SteveQuote.png"
            },
            new VideoSelection
            {
                Patient = Pages.PatientBrenda,
                PatientName = "Brenda",
                PullQuote = "“I feel like my whole body is in chaos.”",
                BgUrl = "pack://siteoforigin:,,,/Resources/Images/PatientVideos/bgBrenda.png",
                VideoPath = $"pack://siteoforigin:,,,/Resources/Videos/Story/{Settings.Default.StoryVideo_Brenda}",
                PatientQuoteImagePath = "pack://siteoforigin:,,,/Resources/Images/PatientQuotes/BrendaQuote.png"
            },
            new VideoSelection
            {
                Patient = Pages.PatientMikeA,
                PatientName = "Mike A.",
                PullQuote = "“You're not alone.”",
                BgUrl = "pack://siteoforigin:,,,/Resources/Images/PatientVideos/bgMikeA.png",
                VideoPath = $"pack://siteoforigin:,,,/Resources/Videos/Story/{Settings.Default.StoryVideo_MikeA}",
                PatientQuoteImagePath = "pack://siteoforigin:,,,/Resources/Images/PatientQuotes/MikeAQuote.png"
            },
            new VideoSelection
            {
                Patient = Pages.PatientIsrael,
                PatientName = "Israel",
                PullQuote = "“My focus needed to be not on myself, but on helping others...”",
                BgUrl = "pack://siteoforigin:,,,/Resources/Images/PatientVideos/bgIsrael.png",
                VideoPath = $"pack://siteoforigin:,,,/Resources/Videos/Story/{Settings.Default.StoryVideo_Israel}",
                PatientQuoteImagePath = "pack://siteoforigin:,,,/Resources/Images/PatientQuotes/IsraelQuote.png"
            },
            new VideoSelection
            {
                Patient = Pages.PatientDrElmer,
                PatientName = "Dr. Elmer",
                PullQuote = "“Defining OFF periods is probably one of the most important things.”",
                BgUrl = "pack://siteoforigin:,,,/Resources/Images/PatientVideos/bgDrElmer.png",
                VideoPath = $"pack://siteoforigin:,,,/Resources/Videos/Story/{Settings.Default.StoryVideo_DrElmer}",
                PatientQuoteImagePath = "pack://siteoforigin:,,,/Resources/Images/PatientQuotes/DrElmerQuote.png"
            },
            new VideoSelection
            {
                Patient = Pages.PatientDrPahwa,
                PatientName = "Dr. Pahwa",
                PullQuote = "“OFF periods basically varies in different patients.”",
                BgUrl = "pack://siteoforigin:,,,/Resources/Images/PatientVideos/bgDrPahwa.png",
                VideoPath = $"pack://siteoforigin:,,,/Resources/Videos/Story/{Settings.Default.StoryVideo_DrPahwa}",
                PatientQuoteImagePath = "pack://siteoforigin:,,,/Resources/Images/PatientQuotes/DrPahwaQuote.png"
            },
            new VideoSelection
            {
                Patient = Pages.PatientLynn,
                PatientName = "Lynn",
                PullQuote = "“OFF gets in the way of a lot of different areas in my life.”",
                BgUrl = "pack://siteoforigin:,,,/Resources/Images/PatientVideos/bgLynn.png",
                VideoPath = $"pack://siteoforigin:,,,/Resources/Videos/Story/{Settings.Default.StoryVideo_Lynn}",
                PatientQuoteImagePath = "pack://siteoforigin:,,,/Resources/Images/PatientQuotes/LynnQuote.png"
            },
        };
    }
}
