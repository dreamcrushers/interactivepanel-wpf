﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace AcordaKioskWPF.View
{
	/// <summary>
	/// Interaction logic for HotButtonControl.xaml
	/// </summary>
	public partial class HotButtonControl : UserControl
	{
		#region dps

		public bool ButtonsVisible
		{
			get { return (bool)GetValue(ButtonsVisibleProperty); }
			set { SetValue(ButtonsVisibleProperty, value); }
		}
		public static readonly DependencyProperty ButtonsVisibleProperty = DependencyProperty.Register("ButtonsVisible", typeof(bool), typeof(HotButtonControl), new FrameworkPropertyMetadata(true));


		public static Corner GetHotButtonType(Button obj)
		{
			return (Corner)obj.GetValue(HotButtonTypeProperty);
		}
		public static void SetHotButtonType(Button obj, Corner value)
		{
			obj.SetValue(HotButtonTypeProperty, value);
		}
		public static readonly DependencyProperty HotButtonTypeProperty = DependencyProperty.RegisterAttached("HotButtonType",
																											   typeof(Corner),
																											   typeof(HotButtonControl));


		public double ButtonSize
		{
			get { return (double)GetValue(ButtonSizeProperty); }
			set { SetValue(ButtonSizeProperty, value); }
		}
		public static readonly DependencyProperty ButtonSizeProperty = DependencyProperty.Register("ButtonSize",
																								   typeof(double),
																								   typeof(HotButtonControl),
																								   new PropertyMetadata(100.0));
		#endregion

		#region click evt

		public static readonly RoutedEvent HotButtonClickEvent = EventManager.RegisterRoutedEvent("HotButtonClick",
																								 RoutingStrategy.Bubble,
																								 typeof(HotButtonClickEventHandler),
																								 typeof(HotButtonControl));

		public static void AddHotButtonClickHandler(UIElement obj, HotButtonClickEventHandler handler)
		{
			obj.AddHandler(HotButtonClickEvent, handler);
		}
		public static void RemoveHotButtonClickHandler(UIElement obj, HotButtonClickEventHandler handler)
		{
			obj.RemoveHandler(HotButtonClickEvent, handler);
		}

		public event HotButtonClickEventHandler HotButtonClick
		{
			add { AddHotButtonClickHandler(this, value); }
			remove { RemoveHotButtonClickHandler(this, value); }
		}

		#endregion

		public HotButtonControl()
		{
			InitializeComponent();
		}

		private void TripleClick(object sender, RoutedEventArgs e)
		{
			Button src_btn = e.OriginalSource as Button;
			Debug.Assert(src_btn != null);

			Corner c = GetHotButtonType(src_btn);
			RaiseEvent(new HotButtonClickEventArgs(c));
		}
	}

	public delegate void HotButtonClickEventHandler(object sender, HotButtonClickEventArgs args);

	public class HotButtonClickEventArgs : RoutedEventArgs
	{
		public Corner CornerClicked { get; private set; }
		internal HotButtonClickEventArgs(Corner c)
			: base(HotButtonControl.HotButtonClickEvent)
		{
			CornerClicked = c;
		}
	}

	[ValueConversion(typeof(Corner), typeof(VerticalAlignment))]
	public class HotButtonVerticalAlignmentConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			Corner val = (Corner)value;
			return val == Corner.TopLeft || val == Corner.TopRight ? VerticalAlignment.Top : VerticalAlignment.Bottom;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}

	[ValueConversion(typeof(Corner), typeof(HorizontalAlignment))]
	public class HotButtonHorizontalAlignmentConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			Corner val = (Corner)value;
			return val == Corner.TopLeft || val == Corner.BottomLeft ? HorizontalAlignment.Left : HorizontalAlignment.Right;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}

	public enum Corner
	{
		TopLeft,
		TopRight,
		BottomLeft,
		BottomRight
	}
}