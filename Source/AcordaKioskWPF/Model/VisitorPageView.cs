namespace AcordaKioskWPF.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class VisitorPageView
    {
        [Key]
        public int StationID { get; set; }

        public string StationName { get; set; }

        public string BadgeNum { get; set; }

        public DateTime CaptureTime { get; set; }

        public string Section { get; set; }

        public int? PageNum { get; set; }
    }
}
