﻿using System.Windows;
using System.Windows.Media;

namespace AcordaKioskWPF.Utils
{
	public class ImageButton
	{
		#region Default

		/// <summary>
		/// An attached dependency property which provides an
		/// <see cref="ImageSource" /> for arbitrary WPF elements.
		/// </summary>
		public static readonly DependencyProperty DefaultProperty =
			DependencyProperty.RegisterAttached("Default", typeof(ImageSource), typeof(ImageButton),
			new FrameworkPropertyMetadata((ImageSource)null));

		/// <summary>
		/// Gets the <see cref="DefaultProperty"/> for a given
		/// <see cref="DependencyObject"/>, which provides an
		/// <see cref="ImageSource" /> for arbitrary WPF elements.
		/// </summary>
		public static ImageSource GetDefault(DependencyObject obj)
		{
			return (ImageSource)obj.GetValue(DefaultProperty);
		}

		/// <summary>
		/// Sets the attached <see cref="DefaultProperty"/> for a given
		/// <see cref="DependencyObject"/>, which provides an
		/// <see cref="ImageSource" /> for arbitrary WPF elements.
		/// </summary>
		public static void SetDefault(DependencyObject obj, ImageSource value)
		{
			obj.SetValue(DefaultProperty, value);
		}

		#endregion

		#region Down

		/// <summary>
		/// An attached dependency property which provides an
		/// <see cref="ImageSource" /> for arbitrary WPF elements.
		/// </summary>
		public static readonly DependencyProperty DownProperty =
			DependencyProperty.RegisterAttached("Down", typeof(ImageSource), typeof(ImageButton), 
			new FrameworkPropertyMetadata((ImageSource)null));

		/// <summary>
		/// Gets the <see cref="DownProperty"/> for a given
		/// <see cref="DependencyObject"/>, which provides an
		/// <see cref="ImageSource" /> for arbitrary WPF elements.
		/// </summary>
		public static ImageSource GetDown(DependencyObject obj)
		{
			return (ImageSource)obj.GetValue(DownProperty);
		}

		/// <summary>
		/// Sets the attached <see cref="DownProperty"/> for a given
		/// <see cref="DependencyObject"/>, which provides an
		/// <see cref="ImageSource" /> for arbitrary WPF elements.
		/// </summary>
		public static void SetDown(DependencyObject obj, ImageSource value)
		{
			obj.SetValue(DownProperty, value);
		}

		#endregion

		#region Disabled

		/// <summary>
		/// An attached dependency property which provides an
		/// <see cref="ImageSource" /> for arbitrary WPF elements.
		/// </summary>
		public static readonly DependencyProperty DisabledProperty =
			DependencyProperty.RegisterAttached("Disabled", typeof(ImageSource), typeof(ImageButton),
			new FrameworkPropertyMetadata((ImageSource)null));

		/// <summary>
		/// Gets the <see cref="DisabledProperty"/> for a given
		/// <see cref="DependencyObject"/>, which provides an
		/// <see cref="ImageSource" /> for arbitrary WPF elements.
		/// </summary>
		public static ImageSource GetDisabled(DependencyObject obj)
		{
			return (ImageSource)obj.GetValue(DisabledProperty);
		}

		/// <summary>
		/// Sets the attached <see cref="DisabledProperty"/> for a given
		/// <see cref="DependencyObject"/>, which provides an
		/// <see cref="ImageSource" /> for arbitrary WPF elements.
		/// </summary>
		public static void SetDisabled(DependencyObject obj, ImageSource value)
		{
			obj.SetValue(DisabledProperty, value);
		}

        #endregion
    }
}
