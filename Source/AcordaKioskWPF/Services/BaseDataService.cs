using System.Collections.Generic;
using System.Linq;
using AcordaKioskWPF.Model;
using NLog;

namespace AcordaKioskWPF.Services
{
    public class BaseDataService : IDataService
    {
        protected readonly Logger Logger = LogManager.GetCurrentClassLogger();
        protected List<string> WatchedPatients = new List<string>();
        protected List<KioskSurveyQuestion> Answers = new List<KioskSurveyQuestion>();

        protected string UserType { get; set; }

        public virtual void UserLogin(string badgeId, string stationName)
        {
            Logger.Debug("Clearing old session data");
            //WatchedPatients = new List<string>(); //was causing issues with the Watched video check mark
            //UserType = "";    // user type is fetched manually from the server at the same time as login
            Answers = new List<KioskSurveyQuestion>();
        }

        public virtual void WriteUserAnswer(string badgeId, int questionId, string questionText, int answerId, string answerText) { throw new System.NotImplementedException(); }
        public virtual KioskSurveyQuestion GetUserAnswer(string badgeId, int questionId) { throw new System.NotImplementedException(); }
        public virtual void ClearUserAnswers(string badgeId, int questionId) { throw new System.NotImplementedException(); }
        public virtual bool HasSurvey(string badgeId) { throw new System.NotImplementedException(); }
        public virtual void WritePageview(string badgeId, string stationName, string section, int pageNum) { throw new System.NotImplementedException(); }

        public virtual void MarkPatientWatched(string patientName)
        {
            WatchedPatients.Add(patientName);
        }

        public virtual void ClearCachedAnswers()
        {
            Answers.Clear();
        }

        public virtual void ClearWatchedPatients()
        {
            WatchedPatients.Clear();
        }

        public virtual void WriteImportantVideo(string badgeId, string video) { throw new System.NotImplementedException(); }

        public virtual string[] GetWatchedPatients() { return WatchedPatients.ToArray(); }

        public virtual void SetUserType(string userType) { UserType = userType; }
        public virtual string GetUserType() { return UserType; }

        public virtual void CacheSurveyAnswer(KioskSurveyQuestion answer)
        {
            CacheSurveyAnswer(answer, true);
        }
        public virtual void CacheSurveyAnswer(KioskSurveyQuestion answer, bool removeExisting)
        {
            if(removeExisting)
                Answers.RemoveAll(a => a.QuestionId == answer.QuestionId);
            Answers.Add(answer);
        }
        public virtual KioskSurveyQuestion GetCachedAnswer(int questionId)
        {
            return Answers.FirstOrDefault(a => a.QuestionId == questionId);
        }

        public virtual void PersistCachedAnswers(string badgeId)
        {
            foreach (var question in Answers)
            {
                ClearUserAnswers(badgeId, question.QuestionId);
                foreach (var selected_answer in question.SelectedAnswers)
                {
                    if(!question.AnswerMapping.ContainsKey(selected_answer)) continue;
                    WriteUserAnswer(badgeId, question.QuestionId, question.QuestionText, question.AnswerMapping[selected_answer], selected_answer);
                }
            }
        }
    }
}