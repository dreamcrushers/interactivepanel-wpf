﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcordaKioskWPF.Messages
{
    public class ImportantVideoMessage
    {
        public string BadgeNum { get; set; }
        public string Video { get; set; }
    }
}
