﻿using AcordaKioskWPF.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcordaKioskWPF.Utils
{
    public static class DefaultVideoSelector
    {
        public static int GetDefaultVideoIndex()
        {
            var defaultVideoIndex = GetVideoIndex(Settings.Default.DefaultPortrait);

            return defaultVideoIndex;
        }

        public static int GetVideoIndex(string portrait)
        {
            var videoIndex = 0;

            switch (portrait)
            {
                case DefaultPortrait.MikeB:
                    videoIndex = 0;
                    break;
                case DefaultPortrait.Steve:
                    videoIndex = 1;
                    break;
                case DefaultPortrait.Brenda:
                    videoIndex = 2;
                    break;
                case DefaultPortrait.MikeA:
                    videoIndex = 3;
                    break;
                case DefaultPortrait.Israel:
                    videoIndex = 4;
                    break;
                case DefaultPortrait.DrElmer:
                    videoIndex = 5;
                    break;
                case DefaultPortrait.DrPahwa:
                    videoIndex = 6;
                    break;
                case DefaultPortrait.Lynn:
                    videoIndex = 7;
                    break;
                default:
                    videoIndex = 0;
                    break;
            }

            return videoIndex;
        }
    }
}
