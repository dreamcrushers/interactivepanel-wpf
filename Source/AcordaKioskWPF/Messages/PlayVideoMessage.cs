﻿namespace AcordaKioskWPF.Messages
{
    public class PlayVideoMessage
    {
        public string VideoPath { get; set; }
        public string VideoName { get; set; }
    }
}
