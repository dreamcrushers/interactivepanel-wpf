﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using AcordaKioskWPF.Messages;
using AcordaKioskWPF.Model;
using AcordaKioskWPF.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace AcordaKioskWPF.ViewModel
{
    public class SelectOffPeriodsViewModel : ViewModelBase
    {
        private bool _isLoading;

        private RelayCommand _loadedCommand;
        public RelayCommand LoadedCommand => _loadedCommand ?? (_loadedCommand = new RelayCommand(() =>
        {
            SetQuestionTextFromType();

            _isLoading = true;

            // clear old selected
            var selected = _options.FirstOrDefault(n => n.IsSelected);
            if (selected != null) selected.IsSelected = false;

            _isLoading = false;

            // reset any selected values from this session
            var answer = _dataService.GetCachedAnswer(KioskSurveyQuestion.Off.QuestionId);
            if (answer == null || answer.SelectedAnswers.Count == 0) return;
            foreach (var selected_answer in answer.SelectedAnswers)
            {
                var spec = _options.FirstOrDefault(s => s.Value == selected_answer);
                if (spec != null) spec.IsSelected = true;
            }
        }));

        private string _questionText;
        public string QuestionText
        {
            get { return _questionText; }
            set
            {
                if (_questionText == value) return;
                _questionText = value;
                RaisePropertyChanged();
            }
        }

        private string _questionNumberText;
        public string QuestionNumberText
        {
            get { return _questionNumberText; }
            set
            {
                if (_questionNumberText == value) return;
                _questionNumberText = value;
                RaisePropertyChanged();
            }
        }

        private ObservableCollection<SelectedObject> _options;
        public ObservableCollection<SelectedObject> Options
        {
            get
            {
                if (_options != null) return _options;
                _options = new ObservableCollection<SelectedObject>(
                    KioskSurveyQuestion
                        .Off
                        .AnswerMapping
                        .Select(k => new SelectedObject(k.Key, OptionPropertyChanged)).ToArray()
                    );
                return _options;
            }
            set
            {
                _options = value;
                RaisePropertyChanged();
            }
        }

        // ensure only one name can be selected at once
        private void OptionPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "IsSelected" || _isLoading) return;

            // if this is the thing we've selected, go ahead and send the navigation message and cache to the repo
            // if we've deselected all the items, clear the message so we fallback to default logic, clear the cached answer
            // if we've selected something else, this is just an event fired killing the old selection, so ignore
            var s = (SelectedObject)sender;
            if (s.IsSelected)
            {
                var answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.Off);
                answer.SelectedAnswers.Add(s.Value);
                _dataService.CacheSurveyAnswer(answer);

                Messenger.Default.Send(new SurveyQuestionSelectedMessage
                {
                    NextPage = s.Value == "No" ? Pages.FinishSurvey : ""
                });

                foreach (var spec in Options)
                {
                    if (s.Value != spec.Value) spec.IsSelected = false;
                }
            }
            else if (!_options.Any(o => o.IsSelected))
            {
                Messenger.Default.Send(new SurveyQuestionSelectedMessage());
                var answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.Off);
                _dataService.CacheSurveyAnswer(answer);
            }
        }

        public void SetQuestionTextFromType()
        {
            var text = "";
            switch (UserType.IdFromType(_dataService.GetUserType()))
            {
                case UserType.PWP:
                    text += "Do you experience OFF periods?";
                    QuestionNumberText = $"#1 of 5";
                    break;
                case UserType.CAREGIVER:
                    text += "Does your loved one experience OFF periods?";
                    QuestionNumberText = $"#1 of 5";
                    break;
                case UserType.HCP:
                    text += "Are you familiar with OFF periods?";
                    QuestionNumberText = $"#1 of 5";
                    break;
                case UserType.OTHER:
                    text += "Do you know what OFF periods are?";
                    QuestionNumberText = $"#1 of 5";
                    break;
            }
            QuestionText = text;
        }

        private readonly IDataService _dataService;
        public SelectOffPeriodsViewModel(IDataService dataService)
        {
            _dataService = dataService;
        }
    }
}
