﻿using System;
using System.Collections.Generic;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Threading;
using AcordaKioskWPF.Utils;
using NLog;

namespace AcordaKioskWPF.View
{
	/// <summary>
	/// Interaction logic for DebugTextControl.xaml
	/// </summary>
	public partial class DebugTextControl
	{
		// This is used to catch messages before the debug console has been instantiated, so when it is, it will show the messages
		public static void Init()
		{
			NLogEventTarget.NLogEvent += NLogEventTarget_NLogEvent;
		}

		private static readonly List<NLogEventTargetArgs> QueuedMessages = new List<NLogEventTargetArgs>();
	    private static void NLogEventTarget_NLogEvent(object sender, NLogEventTargetArgs e)
		{
			QueuedMessages.Add(e);
		}

		public DebugTextControl()
		{
			NLogEventTarget.NLogEvent -= NLogEventTarget_NLogEvent;

			InitializeComponent();
			NLogEventTarget.NLogEvent += ThreadsafeWriteToDebugConsole;

			foreach (var message in QueuedMessages)
			{
				ThreadsafeWriteToDebugConsole(this, message);
			}
			QueuedMessages.Clear();
		}

	    private delegate void DebugConsoleWriteDelegate(string message, LogEventInfo info);

		private void ThreadsafeWriteToDebugConsole(object sender, NLogEventTargetArgs e)
		{
			Dispatcher.BeginInvoke(new DebugConsoleWriteDelegate(WriteToDebugConsole), DispatcherPriority.Background, e.CompiledMessage, e.EventInfo);
		}

		public void WriteToDebugConsole(string message, LogEventInfo info)
		{
			var tr = new TextRange(DebugText.Document.ContentEnd, DebugText.Document.ContentEnd)
			{
				Text = message + '\r'
			};

			if (info.Exception != null)
				tr.Text += $"==============EXCEPTION==============\r{info.Exception}\r=====================================\r";

			var level = info.Level;
			if (level == LogLevel.Debug)
			{
				tr.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.Gray);
			}
			else if (level == LogLevel.Trace)
			{
				tr.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.Lime);
			}
			else if (level == LogLevel.Info)
			{
				tr.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.LimeGreen);
			}
			else if (level == LogLevel.Warn)
			{
				tr.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.Yellow);
			}
			else if (level == LogLevel.Error || level == LogLevel.Fatal)
			{
				tr.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.Red);
			}

			DebugText.ScrollToEnd();
		}
	}
}