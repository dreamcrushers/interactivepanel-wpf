namespace AcordaKioskWPF.Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class AcordaModel : DbContext
    {
        public AcordaModel()
            : base("name=AcordaModel")
        {
        }

        public virtual DbSet<RFIDAssignment> RFIDAssignments { get; set; }
        public virtual DbSet<RFIDReadTag> RFIDReadTags { get; set; }
        public virtual DbSet<SurveyQuestionAnswer> SurveyQuestionAnswers { get; set; }
        public virtual DbSet<VisitorLogin> VisitorLogins { get; set; }
        public virtual DbSet<VisitorPageView> VisitorPageViews { get; set; }
        public virtual DbSet<Visitor> Visitors { get; set; }
        public virtual DbSet<VisitorVIP> VisitorVIPs { get; set; }
        public virtual DbSet<VisitorImportantVideo> VisitorImportantVideos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RFIDAssignment>()
                .Property(e => e.Consumer)
                .IsFixedLength();

            modelBuilder.Entity<RFIDAssignment>()
                .Property(e => e.MacAddress)
                .IsFixedLength();

            modelBuilder.Entity<VisitorVIP>()
                .Property(e => e.TeamLevel)
                .IsFixedLength();

            modelBuilder.Entity<VisitorVIP>()
                .Property(e => e.SiteType)
                .IsFixedLength();

            modelBuilder.Entity<VisitorVIP>()
                .Property(e => e.InvFlag)
                .IsFixedLength();
        }
    }
}
