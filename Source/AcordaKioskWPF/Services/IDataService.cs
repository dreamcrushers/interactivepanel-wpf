﻿using System.Threading.Tasks;
using AcordaKioskWPF.Model;

namespace AcordaKioskWPF.Services
{
    public interface IDataService
    {
        void UserLogin(string badgeId, string stationName);
        void WriteUserAnswer(string badgeId, int questionId, string questionText, int answerId, string answerText);
        KioskSurveyQuestion GetUserAnswer(string badgeId, int questionId);
        void ClearUserAnswers(string badgeId, int questionId);
        void ClearWatchedPatients();
        bool HasSurvey(string badgeId);
        void WritePageview(string badgeId, string stationName, string section, int pageNum);
        void WriteImportantVideo(string badgeId, string video);

        void MarkPatientWatched(string patientName);
        string[] GetWatchedPatients();

        void SetUserType(string userType);
        string GetUserType();

        void CacheSurveyAnswer(KioskSurveyQuestion answer);
        void CacheSurveyAnswer(KioskSurveyQuestion answer, bool removeExisting);
        void ClearCachedAnswers();
        KioskSurveyQuestion GetCachedAnswer(int questionId);
        void PersistCachedAnswers(string badgeId);
    }
}
