﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using AcordaKioskWPF.Messages;
using GalaSoft.MvvmLight.Messaging;

namespace AcordaKioskWPF.View
{
    public partial class PatientNick : UserControl
    {
        private bool _vidPlaying;

        public PatientNick()
        {
            InitializeComponent();

            Messenger.Default.Register<NextPageMessage>(this, msg =>
            {
                if (!_vidPlaying) return;

                Messenger.Default.Send(new VideoStateChangedMessage { IsPlaying = false, VideoName = "Nick" });
                NickVideo.Stop();
                _vidPlaying = false;
            });

            Messenger.Default.Register<PushFooterMessage>(this, msg =>
            {
                if (!_vidPlaying) return;
                NickVideo.Pause();
                _vidPlaying = false;
            });
        }

        private void NickVideo_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            PosterImage.Visibility = Visibility.Hidden;
            if (!_vidPlaying)
                NickVideo.Play();
            else
                NickVideo.Pause();
            _vidPlaying = !_vidPlaying;

            Messenger.Default.Send(new VideoStateChangedMessage { IsPlaying = _vidPlaying, VideoName = "Nick" });
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            // preload the video under a poster image so that we don't get any dumb stuttering
            NickVideo.IsMuted = true;
            NickVideo.Play();
            Task.Delay(1000).ContinueWith(t =>
            {
                Dispatcher.Invoke(() =>
                {
                    NickVideo.Pause();
                    NickVideo.IsMuted = false;
                    NickVideo.Position = TimeSpan.Zero;
                    PosterImage.IsEnabled = true;
                });
            });
        }

        private void NickVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send(new VideoStateChangedMessage { IsPlaying = false, VideoName = "Nick" });
            Task.Delay(2000).ContinueWith(t =>
            {
                Dispatcher.Invoke(() =>
                {
                    PosterImage.Visibility = Visibility.Visible;
                    NickVideo.Position = TimeSpan.Zero;
                    NickVideo.Pause();
                    _vidPlaying = false;
                });
            });
        }
    }
}
