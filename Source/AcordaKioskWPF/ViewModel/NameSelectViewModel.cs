﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Threading;
using AcordaKioskWPF.Messages;
using AcordaKioskWPF.Model;
using AcordaKioskWPF.Properties;
using AcordaKioskWPF.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using NLog;
using AcordaKioskWPF.Utils;

namespace AcordaKioskWPF.ViewModel
{
    public class NameSelectViewModel : ViewModelBase
    {
        public const int OTHER_HCP_TYPE_AID = 4;
        public const int QUESTION_1_ID = 0;
        
		#region private Constants

        const int MaxRows = 2;
        const int MaxColumns = 3;

        #endregion

        // handle a manual tap on the screen
        private RelayCommand _submitCommand;
        public RelayCommand SubmitCommand
        {
            get
            {
                return _submitCommand ?? (_submitCommand = new RelayCommand(() =>
                {
                    _refreshTimer.Stop();

                    Messenger.Default.Send(new NextPageMessage(GetType()));

                    var selected = _users.FirstOrDefault(n => n.IsSelected);
                    if (selected == null)
                    {
                        _logger.Error("Leaving rfid name select screenwithout having selected someone!");
                        return;
                    }

                    var hcp_type = _dataService.GetUserAnswer(selected.Visitor.BadgeNum, QUESTION_1_ID);
                    var user_type = hcp_type != null
                        ? hcp_type.SelectedAnswers.First()
                        : KioskSurveyQuestion.HcpType.AnswerMapping.First(a => a.Value == OTHER_HCP_TYPE_AID).Key;

                    Messenger.Default.Send(new NameSelectedMessage { Name = selected.Name, User = selected.Visitor, UserType = user_type });
                    selected.IsSelected = false;

                    _dataService.ClearWatchedPatients();

                    if (_dataService.HasSurvey(selected.Visitor.BadgeNum))
                    {
                        Messenger.Default.Send(new NextPageMessage(Pages.NameConfirmNoSurvey));
                        Messenger.Default.Send(new NameConfirmedMessage { User = selected.Visitor });

                        var default_video = VideoSelection.DefaultSelections[DefaultVideoSelector.GetDefaultVideoIndex()];
                        _dataService.MarkPatientWatched(default_video.PatientName);
                        Messenger.Default.Send(new PlayVideoMessage { VideoPath = default_video.VideoPath, VideoName = default_video.PatientName });
                        
                    }
                }));
            }
        }

        #region users binding & change

        private ObservableCollection<User> _users;
        public ObservableCollection<User> Users
        {
            get
            {
                return _users ?? (_users = new ObservableCollection<User>());
            }
            set
            {
                _users = value;
                RaisePropertyChanged();
            }
        }

        // ensure only one name can be selected at once
        private void UserPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "IsSelected") return;

            // update submit button so it's only visible when we have a name selected
            SubmitEnabled = Users.Any(uz => uz.IsSelected);

            // if selecting, deselect everybody else
            var u = (User)sender;
            if (!u.IsSelected) return;
            foreach (var name in Users)
            {
                if (u.Name != name.Name) name.IsSelected = false;
            }
        }

        private bool _submitEnabled;
        public bool SubmitEnabled
        {
            get { return _submitEnabled; }
            set
            {
                if (_submitEnabled == value) return;
                _submitEnabled = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region rfid refresh

        // refresh rfid tags on view load
        private RelayCommand _viewLoadedCommand;
        public RelayCommand ViewLoadedCommand
        {
            get
            {
                return _viewLoadedCommand ?? (_viewLoadedCommand = new RelayCommand(async () =>
                {
                    Messenger.Default.Register<PopFooterMessage>(this, msg =>
                    {
                        _refreshTimer.Start();
                    });

                    _deadMilliseconds = 0;
                    await UpdateUsers();
                    _refreshTimer.Start();
                }));
            }
        }

        private async Task UpdateUsers()
        {
			///This flag can be replaced via Users.CollectionChanged event as well
            var isUsersUpdated = false;
            var visitors = await _rfidService.GetCurrentVisitors(Settings.Default.RfidIPAddress, Settings.Default.RfidAntenna);
            var visitor_list = visitors as IList<Visitor> ?? visitors.ToList();
            if (!visitor_list.Any())
                _deadMilliseconds += Settings.Default.RfidRefreshMilliseconds;
            else
                _deadMilliseconds = 0;

            var new_users = visitor_list.Select(v => new User($"{v.FirstName} {v.LastName?[0]}", UserPropertyChanged, v)).ToList();
            foreach (var user in Users.ToList())
            {
                if (new_users.Contains(user)) continue;
                user.IsSelected = false;    // ensure a leaving name cannot be still selected in the background
                Users.Remove(user);
				isUsersUpdated = true;
            }
            foreach (var user in new_users)
            {
                if (Users.Contains(user)) continue;
                Users.Add(user);
				isUsersUpdated = true;
            }

			if (isUsersUpdated)
            {
                PopulateDisplayOrder();
            }
            if (_deadMilliseconds >= RESET_MILLISECONDS)
            {
                _refreshTimer.Stop();
                Messenger.Default.Send(new NextPageMessage(Pages.Reset));
            }
        }

		private void PopulateDisplayOrder()
        {
            for (int i = 0; i < Users.Count; i++)
            {
                var cordinate = _gridCoordinates.SingleOrDefault(x => x.Index == i);

                if (cordinate != null)
                {
                    Users[i].IsUserVisible = true;
                    Users[i].Row = cordinate.Row;
                    Users[i].Column = cordinate.Column;
                }
                else
                {
                    Users[i].IsUserVisible = false;
                }
            }
        }
		private int ParseOrder(string input)
        {
            var parsedValue = 0;
            int.TryParse(input, out parsedValue);
            return parsedValue;
        }

        private List<GridCoordinate> _gridCoordinates = new List<GridCoordinate>();

        private void CreateGridCoordinates()
        {
            var sortOrderList = Settings.Default.SortOrder.Split(',').Select(ParseOrder).ToList();

            int row = 0;
            int column = 0;

            if (sortOrderList.Count != 6) throw new ArgumentOutOfRangeException("SortOrder Config entry should have order specified for all 6 users");

            foreach (var item in sortOrderList)
            {
                _gridCoordinates.Add(new GridCoordinate() { Index = item - 1, Row = row, Column = column });

                column++;

                if (column == MaxColumns)
                {
                    column = 0;
                    row++;
                }
            }
        }
        private readonly DispatcherTimer _refreshTimer;
        private async void RefreshRfid(object sender, EventArgs eventArgs)
        {
            await UpdateUsers();
        }

        #endregion

        private readonly IRfidService _rfidService;
        private readonly IDataService _dataService;

        private readonly Logger _logger;

        private int _deadMilliseconds = 0;
        public const int RESET_MILLISECONDS = 4000;

        public NameSelectViewModel(IRfidService rfidService, IDataService dataService)
        {
            _dataService = dataService;
            _rfidService = rfidService;
            _logger = LogManager.GetCurrentClassLogger();
			CreateGridCoordinates();
            _refreshTimer = new DispatcherTimer
            {
                Interval = TimeSpan.FromMilliseconds(Settings.Default.RfidRefreshMilliseconds)
            };
            _refreshTimer.Tick += RefreshRfid;

            // handle other next pages
            Messenger.Default.Register<NextPageMessage>(this, msg =>
            {
                _refreshTimer.Stop();
                if (msg.SourcePage == Pages.Reset)
                {
                    var selected = Users.FirstOrDefault(u => u.IsSelected);
                    if (selected != null)
                        selected.IsSelected = false;
                }

                // only restart the timer when we've popped a footer from ourselves
                // in theory could just keep it running in the background ....
                Messenger.Default.Unregister<PopFooterMessage>(this); 
            });
            Messenger.Default.Register<PushFooterMessage>(this, msg =>
            {
                _refreshTimer.Stop();
            });
        }
    }

    public class User : ObservableObject
    {
        public User(string name, PropertyChangedEventHandler handler, Visitor visitor)
        {
            Name = name;
            PropertyChanged += handler;
            Visitor = visitor;
        }
        public string Name { get; }
        public Visitor Visitor { get; set; }

		private int _column;
        public int Column
        {
            get { return _column; }

            set
            {
                if (_column == value) return;
                _column = value;
                RaisePropertyChanged();
            }
        }

        private int _row;
        public int Row
        {
            get { return _row; }

            set
            {
                if (_row == value) return;
                _row = value;
                RaisePropertyChanged();
            }
        }
		
		private bool _isUserVisible;
        public bool IsUserVisible
        {
            get { return _isUserVisible; }

            set
            {
                if (_isUserVisible == value) return;
                _isUserVisible = value;
                RaisePropertyChanged();
            }
        }
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }

            set
            {
                if (_isSelected == value) return;
                _isSelected = value;
                RaisePropertyChanged();
            }
        }

        public override bool Equals(object obj)
        {
            var u2 = obj as User;
            return Name.Equals(u2?.Name, StringComparison.OrdinalIgnoreCase);
        }

        public override int GetHashCode() { return Name.GetHashCode(); }
    }
	/// <summary>
    /// This class hold Grid panel Row and Column index, Index prop is used to Get location from Config 'SortOrder'
    /// </summary>
    internal class GridCoordinate
    {
        public int Index { get; set; }

        public int Column { get; set; }

        public int Row { get; set; }
    }
}