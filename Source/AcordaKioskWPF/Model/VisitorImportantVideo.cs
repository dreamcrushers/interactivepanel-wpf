namespace AcordaKioskWPF.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class VisitorImportantVideo
    {
        [Key]
        public int Id { get; set; }
        public string BadgeNum { get; set; }
        public string Video { get; set; }

    }
}
