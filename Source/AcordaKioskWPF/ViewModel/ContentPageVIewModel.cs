﻿using AcordaKioskWPF.Messages;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace AcordaKioskWPF.ViewModel
{
    public class ContentPageViewModel : ViewModelBase
    {
        public string PageName { get; set; }

        private RelayCommand<string> _nextPageCommand;
        public RelayCommand<string> NextPageCommand
        {
            get
            {
                return _nextPageCommand ?? (_nextPageCommand = new RelayCommand<string>(arg =>
                {
                    Messenger.Default.Send(!string.IsNullOrEmpty(arg)
                        ? new NextPageMessage($"{PageName}+{arg}")
                        : new NextPageMessage(PageName));
                }));
            }
        }

        private RelayCommand<string> _popFooterCommand;
        public RelayCommand<string> PopFooterCommand
        {
            get
            {
                return _popFooterCommand ?? (_popFooterCommand = new RelayCommand<string>(arg =>
                {
                    Messenger.Default.Send(new PopFooterMessage());
                }));
            }
        }

        private RelayCommand<string> _pushFooterCommand;
        public RelayCommand<string> PushFooterCommand
        {
            get
            {
                return _pushFooterCommand ?? (_pushFooterCommand = new RelayCommand<string>(arg =>
                {
                    Messenger.Default.Send(new PushFooterMessage() { PageName = arg });
                }));
            }
        }

        public ContentPageViewModel()
        {
        }
    }
}