﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace AcordaKioskWPF.Utils
{
	public class ImageButtonSourceConverter : IMultiValueConverter
	{
		// Takes an ImageSource and returns it if it's non-null, otherwise returns ImageButton.Default from the button
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			var desired_val = values[0];
		    if (desired_val != null && desired_val != DependencyProperty.UnsetValue) return desired_val;

            var target = values[1] as DependencyObject;
		    return ImageButton.GetDefault(target);
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
