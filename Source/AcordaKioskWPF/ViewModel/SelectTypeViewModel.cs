﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using AcordaKioskWPF.Messages;
using AcordaKioskWPF.Model;
using AcordaKioskWPF.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace AcordaKioskWPF.ViewModel
{
    public class SelectTypeViewModel : ViewModelBase
    {
        public const int HCP_ANSWER_ID = 3;

        private bool _isLoading;    // ignore selection changes while loading

        private RelayCommand _loadedCommand;
        public RelayCommand LoadedCommand
        {
            get
            {
                return _loadedCommand ?? (_loadedCommand = new RelayCommand(() =>
                {
                    _isLoading = true;

                    // clear old selected
                    var selected = _types.FirstOrDefault(n => n.IsSelected);
                    if (selected != null) selected.IsSelected = false;

                    _isLoading = false;

                    // reset any selected values from this session
                    var type = _dataService.GetUserType();
                    selected = _types.FirstOrDefault(t => t.Value == type);
                    if (selected != null) selected.IsSelected = true;
                }));
            }
        }

        public override void Cleanup()
        {
            base.Cleanup();

            // save the hcp type at the data service (locally)
            // default to last one in the list (other) if the user skips the question
            var selected = _types.FirstOrDefault(n => n.IsSelected) ?? _types.Last();
            _dataService.SetUserType(selected.Value);
        }

        private ObservableCollection<SelectedObject> _types;
        public ObservableCollection<SelectedObject> Types
        {
            get
            {
                if (_types != null) return _types;
                _types = new ObservableCollection<SelectedObject>(
                    KioskSurveyQuestion
                        .HcpType
                        .AnswerMapping
                        .Select(k => new SelectedObject(k.Key, TypePropertyChanged)).ToArray()
                    );
                return _types;
            }
            set
            {
                _types = value;
                RaisePropertyChanged();
            }
        }

        private string _questionNumberText;
        public string QuestionNumberText
        {
            get { return _questionNumberText; }
            set
            {
                if (_questionNumberText == value) return;
                _questionNumberText = value;
                RaisePropertyChanged();
            }
        }

        // ensure only one name can be selected at once
        private void TypePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "IsSelected" || _isLoading) return;

            // if this is the thing we've selected, go ahead and send the navigation message
            // if we've deselected all the items, clear the message so we fallback to default logic
            // if we've selected something else, this is just an event fired killing the old selection, so ignore
            var s = (SelectedObject)sender;
            if (s.IsSelected)
            {
                // used to skip or go to the specialty question
                Messenger.Default.Send(new SurveyQuestionSelectedMessage
                {
                    NextPage = s.Value == KioskSurveyQuestion.HcpType.AnswerMapping.First(m => m.Value == HCP_ANSWER_ID).Key ? Pages.GotoSpecialty : ""
                });
                foreach (var type in Types)
                {
                    if (s.Value != type.Value) type.IsSelected = false;
                }
            }
            else if (!_types.Any(o => o.IsSelected))
            {
                Messenger.Default.Send(new SurveyQuestionSelectedMessage());
            }
        }

        private readonly IDataService _dataService;
        public SelectTypeViewModel(IDataService dataService)
        {
            _dataService = dataService;
        }
    }

    public class SelectedObject : ObservableObject
    {
        public SelectedObject(string type, PropertyChangedEventHandler handler)
        {
            Value = type;
            PropertyChanged += handler;
        }
        public string Value { get; set; }

        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }

            set
            {
                if (_isSelected == value) return;
                _isSelected = value;
                RaisePropertyChanged();
            }
        }
    }
}