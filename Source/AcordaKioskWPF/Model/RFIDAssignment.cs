namespace AcordaKioskWPF.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RFIDAssignment")]
    public partial class RFIDAssignment
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string Consumer { get; set; }

        [StringLength(50)]
        public string MacAddress { get; set; }

        public int? AntennaNumber { get; set; }
    }
}
