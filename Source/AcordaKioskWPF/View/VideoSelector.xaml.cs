﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AcordaKioskWPF.Utils;
using AcordaKioskWPF.Messages;
using AcordaKioskWPF.ViewModel;
using GalaSoft.MvvmLight.Messaging;

namespace AcordaKioskWPF.View
{
    /// <summary>
    /// Interaction logic for VideoSelector.xaml
    /// </summary>
    public partial class VideoSelector : UserControl
    {
        public VideoSelector()
        {
            InitializeComponent();
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            //PeopleBox.ScrollIntoView(PeopleBox.SelectedItem);
            var next = true;
            var source = (ListBox)sender;
            var currentSelectedItem = e.AddedItems[0];

            if (e.RemovedItems.Count != 0)
            {
                var previousSelectedItem = e.RemovedItems[0];

                if (source.Items.IndexOf(previousSelectedItem) == source.Items.Count - 1 &&
                    source.Items.IndexOf(currentSelectedItem) == 0)
                {
                    next = true;
                }
                else if (source.Items.IndexOf(previousSelectedItem) == 0  &&
                    source.Items.IndexOf(currentSelectedItem) == source.Items.Count - 1)
                {
                    next = false;
                }

                else if (source.Items.IndexOf(previousSelectedItem)< source.Items.IndexOf(currentSelectedItem))
                {
                    next = true;
                }
                else
                {
                    next = false;
                }
            }

            PeopleBox.AnimateScrollIntoView(PeopleBox.SelectedItem, next);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            PeopleBox.AnimateScrollIntoView(PeopleBox.SelectedItem);
        }

        private void SwipeBehavior_OnSwipeRight(object sender, RoutedEventArgs e)
        {
            var i = 0;
        }

        private void SwipeBehavior_OnSwipeLeft(object sender, RoutedEventArgs e)
        {
            var j = 0;
        }
    }
}
