﻿namespace AcordaKioskWPF.Messages
{
    public class SurveyQuestionSelectedMessage
    {
        public string NextPage { get; set; }
    }
}
