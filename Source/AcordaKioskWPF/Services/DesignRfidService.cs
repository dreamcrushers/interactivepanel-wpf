﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AcordaKioskWPF.Model;

namespace AcordaKioskWPF.Services
{
    public class DesignRfidService : IRfidService
    {
        Random _r = new Random();

        public Task<IEnumerable<Visitor>> GetCurrentVisitors(string rfidReaderIP, int rfidReaderAntenna)
        {
            if (_r.NextDouble() < 1)
            {
                return Task.FromResult(new List<Visitor>(new[]
                {
                    new Visitor {BadgeNum = "1", FirstName = "Adam", LastName = "Wainwright"},
                    new Visitor {BadgeNum = "2", FirstName = "Luke", LastName = "Weaver"},
                    new Visitor {BadgeNum = "3", FirstName = "Mike", LastName = "Leake"},
                    new Visitor {BadgeNum = "4", FirstName = "Jaime", LastName = "Garcia"},
                    new Visitor {BadgeNum = "5", FirstName = "Carlos", LastName = "Martinez"}
                }).AsEnumerable());
            }
            return Task.FromResult(new List<Visitor>().AsEnumerable());
        }
    }
}
