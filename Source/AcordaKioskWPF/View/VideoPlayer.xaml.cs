﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using NLog;
using GalaSoft.MvvmLight.Messaging;
using AcordaKioskWPF.Messages;
using AcordaKioskWPF.ViewModel;

namespace AcordaKioskWPF.View
{
    /// <summary>
    /// Interaction logic for VideoPlayer.xaml
    /// </summary>
    public partial class VideoPlayer : UserControl
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly TranslateTransform _scrubberTransform = new TranslateTransform();

        public VideoPlayer()
        {
            InitializeComponent();
        }

        private void MediaElement_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            _logger.Error(e.ErrorException, $"Patient video failed to play. ex[{ e.ErrorException.Message }]");
            Messenger.Default.Send(new NextPageMessage(typeof(VideoPlayerViewModel)));
        }

        private void MediaElement_MediaOpened(object sender, RoutedEventArgs e)
        {
            _logger.Debug("Patient video media opened");
        }

        private void MediaElement_MediaEnded(object sender, RoutedEventArgs e)
        {
            _logger.Debug("Patient video ended");
            Messenger.Default.Send(new NextPageMessage(typeof(VideoPlayerViewModel)));
        }

        private DispatcherTimer _progressTimer;

        private void VideoPlayer_OnLoaded(object sender, RoutedEventArgs e)
        {
            _logger.Trace("Begin VideoPlayer_OnLoaded(object sender, RoutedEventArgs e)");

            ScrubberHandle.RenderTransform = _scrubberTransform;

            PatientVideo.IsMuted = true;
            PatientVideo.Pause();

            Task.Delay(500).ContinueWith(t =>
            {
                Dispatcher.Invoke(() =>
                {
                    PatientVideo.IsMuted = false;
                    //PatientVideo.Position = TimeSpan.Zero;

                    var anim = new DoubleAnimation(1, 0, TimeSpan.FromMilliseconds(500));
                    anim.Completed += (obj, argus) =>
                    {
                        PatientVideo.Play();
                        _logger.Debug("Playing video");

                        _progressTimer = new DispatcherTimer
                        {
                            Interval = TimeSpan.FromMilliseconds(50)
                        };
                        _progressTimer.Tick += (o, args) =>
                        {
                            if (!PatientVideo.NaturalDuration.HasTimeSpan) return;
                            UpdateVideoPositionHandler();
                        };
                        _progressTimer.IsEnabled = true;
                    };
                    ContentGrid.BeginAnimation(OpacityProperty, anim);
                });
            });

            _logger.Trace("End VideoPlayer_OnLoaded(object sender, RoutedEventArgs e)");
        }

        private void UpdateVideoPositionHandler()
        {
            var time_ratio = PatientVideo.Position.TotalMilliseconds / PatientVideo.NaturalDuration.TimeSpan.TotalMilliseconds;
            _scrubberTransform.X = SCRUB_BAR_SIZE * time_ratio;  // position where the handle sits at the end of the scrub bar
        }

        public const int PLAY_CIRCLE_SIZE = 42;
        public const int SCRUB_BAR_SIZE = 961;

        private void Image_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            var scrub_to = e.GetPosition((Image)sender).X - PLAY_CIRCLE_SIZE; // size of play circle on the left

            if (scrub_to < 0)
            {
                ToggleVideo();
                return;
            }

            var ratio = scrub_to / SCRUB_BAR_SIZE;
            PatientVideo.Position = TimeSpan.FromMilliseconds( PatientVideo.NaturalDuration.TimeSpan.TotalMilliseconds * ratio );

            UpdateVideoPositionHandler();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            PatientVideo.Stop();
            _progressTimer.Stop();
        }

        bool videoIsPlaying = true;
        private void PatientVideo_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ToggleVideo();
            UpdateVideoPositionHandler();  
        }

        private void ToggleVideo()
        {
            if (videoIsPlaying)
            {
                PatientVideo.Pause();
                _progressTimer.IsEnabled = false;
                videoIsPlaying = false;
                btnVideoScrubber.Source = new BitmapImage(new Uri("pack://siteoforigin:,,,/Resources/btVideoScrubber.png"));
            }
            else
            {
                PatientVideo.Play();
                _progressTimer.IsEnabled = true;
                videoIsPlaying = true;
                btnVideoScrubber.Source = new BitmapImage(new Uri("pack://siteoforigin:,,,/Resources/btVideoScrubber_Pause.png"));
            }
        }
    }
}
