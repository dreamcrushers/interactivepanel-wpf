﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Data;
using System.Windows.Threading;
using AcordaKioskWPF.Utils;
using NLog;

namespace AcordaKioskWPF
{
    public partial class App : Application
    {
        private Mutex _mutex;

        protected override void OnStartup(StartupEventArgs e)
        {
            if (!ApplicationHelper.InitLogging())
                Shutdown(ApplicationExitCode.NLOG_FAILURE);
            else if ((_mutex = ApplicationHelper.CheckForMultipleInstances()) == null)
                Shutdown(ApplicationExitCode.MUTEX_FAILURE);
            else
            {
                PresentationTraceSources.Refresh();
                PresentationTraceSources.DataBindingSource.Listeners.Add(new ConsoleTraceListener());
                //PresentationTraceSources.DataBindingSource.Listeners.Add(new DebugTraceListener());
                PresentationTraceSources.DataBindingSource.Switch.Level = SourceLevels.Warning | SourceLevels.Error;

                base.OnStartup(e);
            }
        }

        private void Application_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            var logger = LogManager.GetCurrentClassLogger();
            logger.Error(e.Exception, $"Unhandled exception! Please take care of me: ex[{ e.Exception.Message }]");
            e.Handled = true;
        }
    }

    public class DebugTraceListener : TraceListener
    {
        public override void Write(string message)
        {
        }

        public override void WriteLine(string message)
        {
            Debugger.Break();
        }
    }

    public class DebugDataBindingConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Debugger.Break();
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Debugger.Break();
            return value;
        }
    }
}
