namespace AcordaKioskWPF.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Visitor
    {
        [Key]
        public int YipBadgeID { get; set; }

        public string Salu { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Country { get; set; }

        public string Zipcode { get; set; }

        public string BadgeNum { get; set; }

        public string RFIDTagNum { get; set; }

        public DateTime CaptureTime { get; set; }

        public int IsVIP { get; set; }

        public string Notes { get; set; }

        public string ShowDataJson { get; set; }

        public int VIP_ID { get; set; }

        public int Subscribe { get; set; }
    }
}
