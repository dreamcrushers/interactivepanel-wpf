﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcordaKioskWPF.Utils
{
    public static class DefaultPortrait
    {
        public const string Brenda = "Brenda";
        public const string Lynn = "Lynn";
        public const string DrElmer = "DrElmer";
        public const string DrPahwa = "DrPahwa";
        public const string Israel = "Israel";
        public const string MikeA = "MikeA";
        public const string MikeB = "MikeB";
        public const string Steve = "Steve";
    }
}
