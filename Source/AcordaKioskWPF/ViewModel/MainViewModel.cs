using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Threading;
using AcordaKioskWPF.Messages;
using AcordaKioskWPF.Model;
using AcordaKioskWPF.Properties;
using AcordaKioskWPF.Services;
using AcordaKioskWPF.Utils;
using AcordaKioskWPF.View;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using NLog;
using Application = System.Windows.Application;

namespace AcordaKioskWPF.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        #region Main Window Load & Display

        private WindowStyle _showToolbar = WindowStyle.None;
        public WindowStyle ShowToolbar
        {
            get { return _showToolbar; }
            set
            {
                if (_showToolbar == value) return;
                _showToolbar = value;
                RaisePropertyChanged();
            }
        }

        private int _left;
        public int Left
        {
            get { return _left; }
            set
            {
                if (_left == value) return;
                _left = value;
                RaisePropertyChanged();
            }
        }

        private int _top;
        public int Top
        {
            get { return _top; }
            set
            {
                if (_top == value) return;
                _top = value;
                RaisePropertyChanged();
            }
        }

        // update window position on load
        private RelayCommand _setWindowPositionCommand;
        public RelayCommand SetWindowPositionCommand
        {
            get
            {
                return _setWindowPositionCommand ?? (_setWindowPositionCommand = new RelayCommand(() =>
                {
                    var console = DebugConsole.Current; // init debug console

                    if (!Settings.Default.LaunchOnSecondMonitor) return;

                    var second_monitor = Screen.AllScreens.FirstOrDefault(s => !s.Primary);
                    if (second_monitor == null) return;

                    var area = second_monitor.WorkingArea;
                    var left = area.Left;
                    var top = area.Top;

                    // transform for multiple dpi windows in win 10
                    var transform = PresentationSource.FromVisual(Application.Current.MainWindow)?.CompositionTarget?.TransformToDevice;
                    if (transform != null)
                    {
                        left = (int)Math.Round(left / transform.Value.M11);
                        top = (int)Math.Round(top / transform.Value.M22);
                    }

                    Left = left;
                    Top = top;
                }));
            }
        }

        #endregion

        #region navigation and global state

        private ViewModelBase _currentPageBase;
        public ViewModelBase CurrentPage
        {
            get { return _currentPageBase; }
            set
            {
                if (_currentPageBase == value) return;
                _currentPageBase = value;
                RaisePropertyChanged();
            }
        }

        private string _video;

        // called on a triple click to one of our hidden buttons
        private RelayCommand<HotButtonClickEventArgs> _performHiddenActionCommand;
        public RelayCommand<HotButtonClickEventArgs> PerformHiddenActionCommand
        {
            get
            {
                return _performHiddenActionCommand ?? (_performHiddenActionCommand = new RelayCommand<HotButtonClickEventArgs>(args =>
                {
                    switch (args.CornerClicked)
                    {
                        case Corner.TopRight:
                            _logger.Debug("Reset tripleclick");
                            Messenger.Default.Send(new PopFooterMessage());
                            Messenger.Default.Send(new NextPageMessage(Pages.Reset));
                            break;
                        case Corner.TopLeft:
                            _logger.Debug("Important video logged tripleclick");

                            if(_currentUser != null)
                            {
                                Messenger.Default.Send(new ImportantVideoMessage {
                                    BadgeNum = _currentUser.BadgeNum,
                                    Video = _video
                                });
                            }

                            break;
                        case Corner.BottomRight:
                            _logger.Info("Shutdown tripleclick");
                            Application.Current.Shutdown(ApplicationExitCode.NORMAL);
                            break;
                        //case Corner.BottomRight:
                        //    _logger.Debug("Toggle debug console tripleclick");
                        //    DebugConsole.Current.ToggleVisibility();
                        //    break;
                    }
                }));
            }
        }

        #endregion

        #region survey

        private bool _surveyControlsVisible;
        public bool SurveyControlsVisible
        {
            get { return _surveyControlsVisible; }
            set
            {
                if (_surveyControlsVisible == value) return;
                _surveyControlsVisible = value;
                RaisePropertyChanged();
            }
        }

        private bool _surveyCanContinue;
        public bool SurveyCanContinue
        {
            get { return _surveyCanContinue; }
            set
            {
                if (_surveyCanContinue == value) return;
                _surveyCanContinue = value;
                RaisePropertyChanged();
            }
        }

        private int _surveyPageNum;
        public int SurveyPageNum
        {
            get { return _surveyPageNum; }
            set
            {
                if (_surveyPageNum == value) return;
                _surveyPageNum = value;
                RaisePropertyChanged();
            }
        } 
        
        #endregion

        private string _attractLoopPath;
        public string AttractLoopPath
        {
            get { return _attractLoopPath; }
            set
            {
                if (_attractLoopPath == value) return;
                _attractLoopPath = value;
                RaisePropertyChanged();
            }
        }

        private bool _storyPulserVisible;
        public bool StoryPulserVisible
        {
            get { return _storyPulserVisible; }
            set
            {
                if (_storyPulserVisible == value) return;
                _storyPulserVisible = value;
                RaisePropertyChanged();
            }
        }

        private readonly InactivityTimer _timer;
        private readonly IRfidService _rfidService;
        private readonly IDataService _dataService;
        private readonly Logger _logger;
        private readonly DispatcherTimer _rfidCheckTimer;

        public MainViewModel(IRfidService rfidService, IDataService dataService)
        {
            switch(Settings.Default.DefaultPortrait)
            {
                case DefaultPortrait.Brenda:
                    AttractLoopPath = $"pack://siteoforigin:,,,/Resources/Videos/Loop/{Settings.Default.PortraitLoop_Brenda}";
                    break;
                case DefaultPortrait.Israel:
                    AttractLoopPath = $"pack://siteoforigin:,,,/Resources/Videos/Loop/{Settings.Default.PortraitLoop_Israel}";
                    break;
                case DefaultPortrait.Lynn:
                    AttractLoopPath = $"pack://siteoforigin:,,,/Resources/Videos/Loop/{Settings.Default.PortraitLoop_Lynn}";
                    break;
                case DefaultPortrait.MikeA:
                    AttractLoopPath = $"pack://siteoforigin:,,,/Resources/Videos/Loop/{Settings.Default.PortraitLoop_MikeA}";
                    break;
                case DefaultPortrait.MikeB:
                    AttractLoopPath = $"pack://siteoforigin:,,,/Resources/Videos/Loop/{Settings.Default.PortraitLoop_MikeB}";
                    break;
                case DefaultPortrait.DrElmer:
                    AttractLoopPath = $"pack://siteoforigin:,,,/Resources/Videos/Loop/{Settings.Default.PortraitLoop_DrElmer}";
                    break;
                case DefaultPortrait.DrPahwa:
                    AttractLoopPath = $"pack://siteoforigin:,,,/Resources/Videos/Loop/{Settings.Default.PortraitLoop_DrPahwa}";
                    break;
                default:
                    break;
            }

            ShowToolbar = Settings.Default.ShowToolbar ? WindowStyle.SingleBorderWindow : WindowStyle.None;

            // rfid! and logging!
            _rfidService = rfidService;
            _dataService = dataService;
            _logger = LogManager.GetCurrentClassLogger();
            _logger.Info("Setting up main vm");

            // navigation!
            Messenger.Default.Register<NextPageMessage>(this, NavigateToNextPage);

            // start page!
            var locator = new ViewModelLocator();
            CurrentPage = locator.AttractLoop;
            SurveyPageNum = 0;

            // inactivity!
            _timer = new InactivityTimer(TimeSpan.FromSeconds(Settings.Default.TimeoutSeconds), true);
            _timer.UserInactive += async (sender, args) =>
            {
                _logger.Info("Got inactivity timeout");
                var rfid_users = await _rfidService.GetCurrentVisitors(Settings.Default.RfidIPAddress, Settings.Default.RfidAntenna);
                if (_currentUser == null || rfid_users.All(r => r.BadgeNum != _currentUser.BadgeNum))
                {
                    _logger.Debug("Current user not present, resetting");
                    Messenger.Default.Send(new NextPageMessage(Pages.Reset));
                }
                else
                {
                    _logger.Debug("Current user present, won't reset");
                }
            };
            _timer.Start();

            // pulseythings!
            _rfidCheckTimer = new DispatcherTimer()
            {
                Interval = TimeSpan.FromMilliseconds(1000)
            };
            _rfidCheckTimer.Tick += async (sender, args) =>
            {
                var rfids = await _rfidService.GetCurrentVisitors(Settings.Default.RfidIPAddress, Settings.Default.RfidAntenna);
                StoryPulserVisible = rfids.Any() && CurrentPage.GetType() == typeof(AttractLoopViewModel);
            };
            _rfidCheckTimer.IsEnabled = true;

            // messaging!
            Messenger.Default.Register<NameConfirmedMessage>(this, UserLogin);
            //Messenger.Default.Register<SurveyFinishedMessage>(this, WriteSurveyAnswers);
            Messenger.Default.Register<VideoStateChangedMessage>(this, msg =>
            {
                if (_currentUser == null) return;
                Task.Run(() =>
                {
                    _dataService.WritePageview(_currentUser.BadgeNum, Settings.Default.StationName, "PatientVideo_" + msg.VideoName, msg.IsPlaying ? 1 : 2);
                    _video = msg.VideoName;
                });
            });
            Messenger.Default.Register<SurveyQuestionSelectedMessage>(this, msg =>
            {
                SurveyCanContinue = true;
                _currentSurveyQuestion = msg;
            });
            Messenger.Default.Register<AnswerAcceptedMessage>(this, msg =>
            {
                SurveyCanContinue = false;

                UpdateQuestionIdsWithAnswer();

                var page_name = !string.IsNullOrEmpty(_currentSurveyQuestion?.NextPage)
                    ? _currentSurveyQuestion.NextPage
                    : CurrentPage.GetType().ToString();

                Messenger.Default.Send(new NextPageMessage(page_name));

                if (page_name == Pages.FinishSurvey)
                {
                    Messenger.Default.Send(new SurveyFinishedMessage());
                }
            });
            Messenger.Default.Register<SurveyFinishedMessage>(this, msg =>
            {
                Task.Run(() => {
                    _dataService.PersistCachedAnswers(_currentUser.BadgeNum);
                });

                var default_video = VideoSelection.DefaultSelections[DefaultVideoSelector.GetDefaultVideoIndex()];
                _dataService.MarkPatientWatched(default_video.PatientName);
                Messenger.Default.Send(new PlayVideoMessage { VideoPath = default_video.VideoPath, VideoName = default_video.PatientName });
            });

            Messenger.Default.Register<PreviousQuestionMessage>(this, msg =>
            {
                UpdateQuestionIdsWithAnswer();

                Messenger.Default.Send(new NextPageMessage(CurrentPage.GetType() + "Previous"));
            });

            Messenger.Default.Register<ImportantVideoMessage>(this, msg =>
            {
                if (!string.IsNullOrEmpty(msg.BadgeNum) && !string.IsNullOrEmpty(msg.Video))
                { 
                    Task.Run(() => {
                        _dataService.WriteImportantVideo(msg.BadgeNum, msg.Video); 
                    });
                }
            });

            Messenger.Default.Register<NameSelectedMessage>(this, msg =>
            {
                UserType = msg.UserType;
                _currentUser = msg.User;
            });
        }

        private void UpdateQuestionIdsWithAnswer()
        {
            var questionIdWithAnswers = string.Empty;
            for (var i = 3; i <= 7; i++)
            {
                var questionAnswer = _dataService.GetCachedAnswer(i);
                if (questionAnswer != null)
                {
                    if (questionAnswer.SelectedAnswers.Count > 0)
                    {
                        questionIdWithAnswers += questionAnswer.QuestionId;
                    }
                }
            }

            QuestionIdWithAnswer = questionIdWithAnswers;
        }

        private SurveyQuestionSelectedMessage _currentSurveyQuestion;

        #region data

        private Visitor _currentUser;
        private string _userType;
        public string UserType
        {
            get { return _userType; }
            set
            {
                if (_userType == value) return;
                _userType = value;
                RaisePropertyChanged();
            }
        }

        private string _questionIdWithAnswer;
        public string QuestionIdWithAnswer
        {
            get { return _questionIdWithAnswer; }
            set
            {
                if (_questionIdWithAnswer == value) return;
                _questionIdWithAnswer = value;
                RaisePropertyChanged();
            }
        }

        private void UserLogin(NameConfirmedMessage msg)
        {
            _logger.Info($"User login with badgeid[{ msg.User.BadgeNum }]");
            Task.Run(() =>
            {
                _currentUser = msg.User;
                _dataService.UserLogin(msg.User.BadgeNum, Settings.Default.StationName);
                _logger.Debug("User login written");
            });
        }

        #endregion

        // page numbers
        //0 - Reset
        //1 - Video started
        //2 - Video stopped

        //3 - Survey_Question1
        //4 - Survey_Question2
        //5 - Survey_Question3
        //6 - Survey_Question4
        //7 - Survey_Question5
        //8 - Survey_Completed

        //9 - PatientVideo_Carousel

        //10 - PatientVideo_Brenda
        //11 - PatientVideo_Israel
        //12 - PatientVideo_DrElmer
        //13 - PatientVideo_DrPahwa
        //14 - PatientVideo_MikeA
        //15 - PatientVideo_MikeB
        //16 - PatientVideo_Lynn
 


        private void NavigateToNextPage(NextPageMessage msg)
        {
            UpdateQuestionIdsWithAnswer();

            _logger.Debug($"Got request to navigate from page[{ msg.SourcePage }]");
            var page_model = new Dictionary<string, PageRoutingInfo>
            {
                { typeof(AttractLoopViewModel).ToString(),      new PageRoutingInfo( typeof(NameSelectViewModel) ) },
                { typeof(NameSelectViewModel).ToString(),       new PageRoutingInfo( typeof(NameConfirmViewModel) )         { PageNum = 1 } },

                { Pages.NameConfirmNoSurvey,                    new PageRoutingInfo( typeof(VideoPlayerViewModel) )         { PageNum = 7 } },

                { Pages.NameConfirm,                            new PageRoutingInfo( typeof(SelectOffPeriodsViewModel) )    { SurveyControlsVisible = true, PageNum = 1} },
                { Pages.GotoSpecialty,                          new PageRoutingInfo( typeof(SelectSpecialtyViewModel))      { SurveyControlsVisible = true, PageNum = 1} },
                { typeof(SelectTypeViewModel).ToString(),       new PageRoutingInfo( typeof(SelectSpecialtyViewModel) )     { SurveyControlsVisible = true, PageNum = 1} },
                { typeof(SelectSpecialtyViewModel).ToString(),  new PageRoutingInfo( typeof(SelectOffPeriodsViewModel) )    { SurveyControlsVisible = true, PageNum = 2 } },
                { typeof(SelectOffPeriodsViewModel).ToString(), new PageRoutingInfo( typeof(SelectSymptomsViewModel) )      { SurveyControlsVisible = true, PageNum = 3 } },
                { typeof(SelectSymptomsViewModel).ToString(),   new PageRoutingInfo( typeof(SelectFeelingsViewModel) )      { SurveyControlsVisible = true, PageNum = 4 } },
                { typeof(SelectFeelingsViewModel).ToString(),   new PageRoutingInfo( typeof(SelectWordViewModel) )          { SurveyControlsVisible = true, PageNum = 5 } },
                { typeof(SelectWordViewModel).ToString(),       new PageRoutingInfo( typeof(SelectLimitViewModel) )         { SurveyControlsVisible = true, PageNum = 6 } },
                { typeof(SelectLimitViewModel).ToString(),      new PageRoutingInfo( typeof(VideoPlayerViewModel) )         { SurveyControlsVisible = false, PageNum = 7 } },        

                { Pages.SpecialtyPrevious,                      new PageRoutingInfo( typeof(SelectTypeViewModel) )          { SurveyControlsVisible = true, PageNum = 1} },
                { Pages.OffPrevious,                            new PageRoutingInfo( typeof(SelectSpecialtyViewModel) )     { SurveyControlsVisible = true, PageNum = 2} },
                { Pages.SymptomsPrevious,                       new PageRoutingInfo( typeof(SelectOffPeriodsViewModel) )    { SurveyControlsVisible = true, PageNum = 2} },
                { Pages.FeelingsPrevious,                       new PageRoutingInfo( typeof(SelectSymptomsViewModel) )      { SurveyControlsVisible = true, PageNum = 3} },
                { Pages.WordPrevious,                           new PageRoutingInfo( typeof(SelectFeelingsViewModel) )      { SurveyControlsVisible = true, PageNum = 4} },
                { Pages.LimitPrevious,                          new PageRoutingInfo( typeof(SelectWordViewModel) )          { SurveyControlsVisible = true, PageNum = 5} },

                { Pages.FinishSurvey,                           new PageRoutingInfo( typeof(VideoPlayerViewModel) )         { PageNum = 8 }  },

                { typeof(VideoSelectorViewModel).ToString(),    new PageRoutingInfo( typeof(VideoPlayerViewModel) )         { PageNum = 9 } },
                { typeof(VideoPlayerViewModel).ToString(),      new PageRoutingInfo( typeof(VideoSelectorViewModel) )       { PageNum = 9 } },

                { Pages.Reset,                                  new PageRoutingInfo( typeof(AttractLoopViewModel) )         { PageNum = 0 } }
            };

            var src_page = msg.SourcePage;
            if (!page_model.ContainsKey(src_page))
                throw new ArgumentException($"Trying to navigate from a page with no mapping: { src_page }");

            // handle special cases for the content page that routes by pagename instead of stricly on model-based data template
            var info = page_model[src_page];
            _logger.Debug($"Routing to page with info: { info }");

            var page = (ViewModelBase) ServiceLocator.Current.GetInstance(info.ModelType);
            if (page.GetType() == typeof(ContentPageViewModel))
                ((ContentPageViewModel) page).PageName = info.PageName;

            // this is going to make animations difficult, but right now is the only to refresh the datatemplateselector
            var old_page = CurrentPage;
            CurrentPage = null;
            old_page.Cleanup();
            CurrentPage = page;

            SurveyControlsVisible = info.SurveyControlsVisible;
            if (info.SurveyControlsVisible)
                SurveyPageNum = info.PageNum;

            // log pageview and clear user on a reset
            if (_currentUser != null)
            {
                var sectionName = "Kiosk";
                var realPageNum = 0;
                switch (info.PageNum)
                {
                    case 0:
                        sectionName = "Reset";
                        realPageNum = 0;
                        break;
                    case 1:
                        sectionName = "Survey";
                        realPageNum = 1;
                        break;
                    case 2:
                        sectionName = "Survey";
                        realPageNum = 1;
                        break;
                    case 3:
                        sectionName = "Survey";
                        realPageNum = 2;
                        break;
                    case 4:
                        sectionName = "Survey";
                        realPageNum = 3;
                        break;
                    case 5:
                        sectionName = "Survey";
                        realPageNum = 4;
                        break;
                    case 6:
                        sectionName = "Survey";
                        realPageNum = 5;
                        break;
                    case 7:
                        sectionName = "Survey_Completed";
                        realPageNum = 0;
                        break;
                    case 8:
                        sectionName = "Survey_Completed";
                        realPageNum = 0;
                        break;
                    case 9:
                        sectionName = "PatientVideo_Carousel";
                        realPageNum = 0;
                        break;
                }
                 
                _dataService.WritePageview(_currentUser.BadgeNum, Settings.Default.StationName, sectionName, realPageNum);
            }
            if (src_page == Pages.Reset)
            {
                _currentUser = null;
                _currentSurveyQuestion = null;
                _dataService.ClearCachedAnswers();
            }
        }
    }

    public class Pages
    {
        public static string NameConfirm            = "Name+Yes";
        public static string NameConfirmNoSurvey    = "Name+YesSkipSurvey";
        public static string NameDeny               = "Name+No";

        public static string GotoSpecialty = "GotoSpecialty";

        public static string SpecialtyPrevious      = typeof(SelectSpecialtyViewModel) + "Previous";
        public static string OffPrevious            = typeof(SelectOffPeriodsViewModel) + "Previous";
        public static string SymptomsPrevious       = typeof(SelectSymptomsViewModel) + "Previous";
        public static string FeelingsPrevious       = typeof(SelectFeelingsViewModel) + "Previous";
        public static string WordPrevious           = typeof(SelectWordViewModel) + "Previous";
        public static string LimitPrevious          = typeof(SelectLimitViewModel) + "Previous";

        public static string Question1 = typeof(SelectTypeViewModel).ToString();
        public static string Question2 = typeof(SelectSpecialtyViewModel).ToString();
        public static string Question3 = typeof(SelectOffPeriodsViewModel).ToString();
        public static string Question4 = typeof(SelectSymptomsViewModel).ToString();
        public static string Question5 = typeof(SelectFeelingsViewModel).ToString();
        public static string Question6 = typeof(SelectWordViewModel).ToString();
        public static string Question7 = typeof(SelectLimitViewModel).ToString();

        public static string FinishSurvey = "FinishSurvey";

        public static string PatientMikeB = "PatientMikeB";
        public static string PatientSteve = "PatientSteve";
        public static string PatientBrenda = "PatientBrenda";
        public static string PatientMikeA = "PatientMikeA";
        public static string PatientIsrael = "PatientIsrael";
        public static string PatientDrElmer = "PatientDrElmer";
        public static string PatientDrPahwa = "PatientDrPahwa";
        public static string PatientLynn = "PatientLynn";

        public static string Reset                  = "ResetApp";
    }

    public class PageRoutingInfo
    {
        public PageRoutingInfo(Type modelType) : this(modelType, modelType.ToString())
        {
        }

        public PageRoutingInfo(Type modelType, string pageName)
        {
            ModelType = modelType;
            PageName = pageName;
            ShowFooter = true;
            FooterEnabled = true;
            SurveyControlsVisible = false;
        }

        public Type ModelType { get; set; }
        public string PageName { get; set; }
        public bool ShowFooter { get; set; }
        public bool FooterEnabled { get; set; }
        public int PageNum { get; set; }
        public bool SurveyControlsVisible { get; set; }

        public override string ToString()
        {
            return $"model[{ ModelType }] page[{ PageName }] showFooter[{ ShowFooter }] enableFooter[{ FooterEnabled }]";
        }
    }
}