﻿using System;
using System.Linq;
using AcordaKioskWPF.Model;

namespace AcordaKioskWPF.Services
{
    public class DataService : BaseDataService
    {
        public override void UserLogin(string badgeId, string stationName)
        {
            base.UserLogin(badgeId, stationName);
            Logger.Info($"User login with badge[{ badgeId }] at station[{ stationName }]");
            using (var ctx = new AcordaModel())
            {
                var login = new VisitorLogin
                {
                    BadgeNum = badgeId,
                    CaptureTime = DateTime.Now,
                    StationName = stationName
                };
                ctx.VisitorLogins.Add(login);
                ctx.SaveChanges();
            }
        }

        public override void WriteImportantVideo(string badgeId,string video)
        {
            Logger.Info($"Important video logged for badge[{ badgeId }] video[{ video }]");
            using (var ctx = new AcordaModel())
            {
                try
                {
                    var importantVideo = new VisitorImportantVideo
                    {
                        BadgeNum = badgeId,
                        Video = video,
                    };
                    ctx.VisitorImportantVideos.Add(importantVideo);
                    ctx.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    throw;
                }
            }
        }

        public override void WriteUserAnswer(string badgeId, int questionId, string questionText, int answerId, string answerText)
        {
            Logger.Info($"Survey answer for badge[{ badgeId }] qid[{ questionId }] qtext[{ questionText }] aid[{ answerId }] atext[{ answerText }]");
            using (var ctx = new AcordaModel())
            {
                var ans = new SurveyQuestionAnswer
                {
                    BadgeNum = badgeId,
                    Question = questionText,
                    QuestionID = questionId
                };
                ctx.SurveyQuestionAnswers.Add(ans);
                ans.Answer = answerText;
                ans.AnswerID = answerId;
                ctx.SaveChanges();
            }
        }

        public override KioskSurveyQuestion GetUserAnswer(string badgeId, int questionId)
        {
            Logger.Info($"Getting survey answer for badge[{badgeId}] qid[{questionId}]");
            using (var ctx = new AcordaModel())
            {
                var qa = ctx.SurveyQuestionAnswers.Where(q => q.BadgeNum == badgeId && q.QuestionID == questionId);
                if (!qa.Any()) return null;

                var question = new KioskSurveyQuestion
                {
                    QuestionId = questionId,
                    QuestionText = qa.First().Question,
                    AnswerMapping = qa.ToDictionary(d => d.Answer, d => d.AnswerID),
                    SelectedAnswers = qa.Select(q => q.Answer).ToList()
                };

                return question;
            }
        }

        public override void ClearUserAnswers(string badgeId, int questionId)
        {
            Logger.Info($"Clearing answers for badge[{ badgeId }] and qid[{ questionId }]");
            using (var ctx = new AcordaModel())
            {
                foreach (var qa in ctx.SurveyQuestionAnswers.Where(qa => qa.BadgeNum == badgeId && qa.QuestionID == questionId))
                {
                    ctx.SurveyQuestionAnswers.Remove(qa);
                }
                ctx.SaveChanges();
            }
        }

        public override bool HasSurvey(string badgeId)
        {
            Logger.Info($"Checking to see if survey exists for user with badgeid[{ badgeId }]");
            using (var ctx = new AcordaModel())
            {
                var all_qas = ctx.SurveyQuestionAnswers
                    .Where(qa => qa.BadgeNum == badgeId)
                    .Where(a => a.QuestionID != 0)
                    .Where(a => a.QuestionID != 1);


                return all_qas.ToList().Count > 0;     // 5 survey questions + specialty
            }
        }

        public override void WritePageview(string badgeId, string stationName, string section, int pageNum)
        {
            Logger.Debug($"Writing pageview for badgeid[{ badgeId }] station[{ stationName }] section[{ section }] page[{ pageNum }]");
            using (var ctx = new AcordaModel())
            {
                var pageview = new VisitorPageView
                {
                    PageNum = pageNum,
                    BadgeNum = badgeId,
                    StationName = stationName,
                    CaptureTime = DateTime.Now,
                    Section = section
                };
                ctx.VisitorPageViews.Add(pageview);
                ctx.SaveChanges();
            }
        }
    }
}
