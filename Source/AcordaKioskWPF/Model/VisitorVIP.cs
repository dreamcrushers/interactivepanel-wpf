namespace AcordaKioskWPF.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VisitorVIP")]
    public partial class VisitorVIP
    {
        [Key]
        public int VIP_ID { get; set; }

        [StringLength(50)]
        public string ReportTitle { get; set; }

        [Column(TypeName = "date")]
        public DateTime? RunDate { get; set; }

        [StringLength(50)]
        public string ClientProtocol { get; set; }

        [StringLength(50)]
        public string SponsorName { get; set; }

        [StringLength(10)]
        public string TeamLevel { get; set; }

        [StringLength(50)]
        public string Region { get; set; }

        public string ParentSite { get; set; }

        public string Site { get; set; }

        [StringLength(10)]
        public string SiteType { get; set; }

        public string SiteName { get; set; }

        [StringLength(50)]
        public string PI_Name { get; set; }

        [StringLength(50)]
        public string SiteStatus { get; set; }

        [StringLength(50)]
        public string Role { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        public string FullName { get; set; }

        [StringLength(50)]
        public string MidName { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(50)]
        public string Degree { get; set; }

        [StringLength(10)]
        public string InvFlag { get; set; }

        [StringLength(50)]
        public string AddrType { get; set; }

        public string Addr1 { get; set; }

        public string Addr2 { get; set; }

        public string Addr3 { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(50)]
        public string ZipCode { get; set; }

        [StringLength(50)]
        public string Country { get; set; }

        [StringLength(50)]
        public string WorkPhone { get; set; }

        [StringLength(50)]
        public string FaxPhone { get; set; }

        [StringLength(50)]
        public string CellPhone { get; set; }

        [StringLength(50)]
        public string HomePhone { get; set; }

        [StringLength(50)]
        public string PagerNumber { get; set; }

        [StringLength(50)]
        public string PagerPin { get; set; }

        [StringLength(50)]
        public string AltPhone { get; set; }

        [StringLength(50)]
        public string Email1 { get; set; }

        [StringLength(50)]
        public string Email2 { get; set; }

        [StringLength(50)]
        public string Email3 { get; set; }

        [StringLength(50)]
        public string RecordStatus { get; set; }
    }
}
