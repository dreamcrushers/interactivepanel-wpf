﻿using AcordaKioskWPF.Model;

namespace AcordaKioskWPF.Messages
{
    public class ServerControlsMessage
    {
        public Visitor CurrentUser { get; set; }
    }
}
